const withMDX = require("@next/mdx")({
  extension: /\.mdx?$/,
});

module.exports = withMDX({
  reactStrictMode: true,
  output: "export",
  staticPageGenerationTimeout: 1000,
  experimental: {
    missingSuspenseWithCSRBailout: false,
    workerThreads: false,
    cpus: 1,
  },
  images: {
    loader: "custom",
    loaderFile: "app/_lib/loader.ts",
  },
  trailingSlash: true,
  pageExtensions: ["js", "jsx", "ts", "tsx", "mdx"],
});
