import { AppProvider } from "../components/common/AppProvider";
import { I18n } from "react-polyglot";
import en from "../locales/en.json";
import "@fontsource/roboto/400.css";
import "@fontsource/poppins/400.css";
import "@fontsource/poppins/700.css";
import "@fontsource/playfair-display/900.css";

const locale = "en";
const messages = { en };

export const decorators = [
  (Story) => (
    <I18n locale={locale} messages={messages[locale]}>
      <AppProvider>
        <Story />
      </AppProvider>
    </I18n>
  ),
];

export const parameters = {
  actions: { argTypesRegex: "^on[A-Z].*" },
  controls: {
    matchers: {
      color: /(background|color)$/i,
      date: /Date$/,
    },
  },
};
