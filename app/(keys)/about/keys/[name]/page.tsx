import { Metadata } from "next";
import { Client } from "@notionhq/client";
import { getNotionProperty, retryableRequest } from "app/_lib/notion";

export const metadata: Metadata = {
  title: "CDR: PGP Key",
};

const getPGPKey = async (name: string): Promise<string> => {
  const notion = new Client({ auth: process.env.NOTION_API_KEY });
  const teamDatabaseID = process.env.NOTION_TEAM_DATABASE_ID;
  const response = await retryableRequest(() =>
    notion.databases.query({
      database_id: teamDatabaseID,
      filter: {
        property: "Slug",
        rich_text: {
          equals: name,
        },
      },
    }),
  );

  const result: any = response.results[0];
  const properties: any = {};

  for await (const key of Object.keys(result.properties)) {
    const val = result.properties[key];
    const prop = await getNotionProperty(notion, result.id, val.id);
    properties[key] = prop;
  }

  const pgpKey = properties["PGP Key"]?.[0]?.rich_text?.plain_text ?? "";

  return pgpKey;
};

type PageProps = {
  params: {
    name: string;
  };
};

export default async function Page({ params: { name } }: PageProps) {
  const pgpKey = await getPGPKey(name);

  return (
    <pre>
      {pgpKey.split("\n").map((line: string, index: number) => (
        <span key={index}>
          {line}
          <br />
        </span>
      ))}
    </pre>
  );
}

export async function generateStaticParams() {
  const notion = new Client({ auth: process.env.NOTION_API_KEY });
  const teamDatabaseID = process.env.NOTION_TEAM_DATABASE_ID;
  const response = await retryableRequest(() =>
    notion.databases.query({
      database_id: teamDatabaseID,
      filter: {
        property: "Active",
        checkbox: {
          equals: true,
        },
      },
    }),
  );

  const params = await Promise.all(
    response.results.map(async (result: any) => {
      const properties: any = {};

      for await (const key of Object.keys(result.properties)) {
        const val = result.properties[key];
        const prop = await getNotionProperty(notion, result.id, val.id);
        properties[key] = prop;
      }

      return {
        name: properties.Slug[0].rich_text.plain_text,
      };
    }),
  );

  return params;
}
