import { Roboto, Playfair_Display, Poppins } from "next/font/google";

const roboto = Roboto({
  weight: ["400"],
  subsets: ["latin"],
  display: "swap",
});

const playfair = Playfair_Display({
  weight: ["900"],
  subsets: ["latin"],
  display: "swap",
});

const poppins = Poppins({
  weight: ["400", "700"],
  subsets: ["latin"],
  display: "swap",
});

export const fonts = {
  roboto,
  playfair,
  poppins,
};

export const colors: any = {
  cdrLinkOrange: "#ff7115",
  coreYellow: "#fac942",
  helpYellow: "#fff4d5",
  dwcDarkBlue: "#191847",
  dwcLightGray: "#ededf0",
  hazyMint: "#ecf7f8",
  leafcutterElectricBlue: "#4d6aff",
  leafcutterLightBlue: "#efefff",
  waterbearElectricPurple: "#332c83",
  waterbearLightSmokePurple: "#eff3f8",
  bumpedPurple: "#212058",
  mutedPurple: "#373669",
  neutralNavy: "#102967",
  beige: "#f6f2f1",
  almostBlack: "#33302f",
  white: "#ffffff",
};

export const typography: any = {
  fontFamily: roboto.style.fontFamily,
  h1: {
    fontFamily: playfair.style.fontFamily,
    fontSize: { xs: 40, sm: 60, lg: 80 },
    lineHeight: 1.0625,
    textAlign: "center",
    fontWeight: 900,
    margin: 0,
    color: colors.almostBlack,
  },
  h2: {
    fontFamily: poppins.style.fontFamily,
    fontWeight: 700,
    fontSize: { xs: 30, sm: 38, lg: 45 },
    lineHeight: 1.3,
    textAlign: "center",
    margin: 0,
    color: colors.almostBlack,
  },
  h3: {
    fontFamily: poppins.style.fontFamily,
    fontWeight: 700,
    fontSize: 30,
    lineHeight: "36px",
    textAlign: "center",
    marginTop: 1,
    marginBottom: 2,
    color: colors.almostBlack,
  },
  h4: {
    fontFamily: poppins.style.fontFamily,
    fontWeight: 700,
    fontSize: 18,
    color: colors.almostBlack,
  },
  h5: {
    fontFamily: poppins.style.fontFamily,
    fontWeight: 700,
    fontSize: 16,
    lineHeight: "24px",
    textTransform: "uppercase",
    textAlign: "center",
    margin: 0,
    color: colors.almostBlack,
  },
  h6: {
    fontFamily: poppins.style.fontFamily,
    fontWeight: 700,
    textAlign: "center",
    color: colors.almostBlack,
  },
  body: {
    fontFamily: roboto.style.fontFamily,
    fontSize: 17,
    lineHeight: "26.35px",
    fontWeight: 400,
    margin: 0,
    marginTop: "15px",
    marginBottom: "15px",
    color: colors.almostBlack,
  },
  bodySmall: {
    fontFamily: roboto.style.fontFamily,
    fontSize: 14,
    fontWeight: 400,
    margin: 0,
    color: colors.almostBlack,
  },
  bodyLarge: {
    fontFamily: roboto.style.fontFamily,
    fontSize: 20,
    fontWeight: 400,
    margin: 0,
    color: colors.almostBlack,
  },
};
