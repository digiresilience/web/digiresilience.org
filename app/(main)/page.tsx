import { Metadata } from "next";
import { Header } from "app/_components/home/Header";
import { Pillars } from "app/_components/home/Pillars";
import { Projects } from "app/_components/home/Projects";
import { Tech } from "app/_components/home/Tech";
import { Network } from "app/_components/common/Network";

export const metadata: Metadata = {
  title: "Center for Digital Resilience",
  description: "Building resilient systems to keep civil society safe",
};

export default function Page() {
  return (
    <>
      <Header />
      <Pillars />
      <Projects />
      <Tech />
      <Network />
    </>
  );
}
