import { Metadata } from "next";
import { Container, Box } from "@mui/material";
import { TextHeader } from "app/_components/common/TextHeader";
import { Layout } from "app/_components/common/Layout";
import { t, loader } from "app/_lib/helpers";
import { colors, typography } from "app/_styles/theme";

export const metadata: Metadata = {
  title: "CDR: Code of Conduct",
};

export default function Page() {
  const { beige, bumpedPurple } = colors;
  const { h2, h3, body, bodyLarge } = typography;

  return (
    <>
      <TextHeader text={t("codeOfConduct")} />
      <Container maxWidth="md" sx={{ pb: 8 }}>
        <Box component="p" sx={{ ...bodyLarge }}>
          {t("codeOfConductIntro")}
        </Box>
        {t("codeOfConductIntroDescription")
          .split("\n")
          .map((paragraph: string, index: number) => (
            <Box key={index} component="p" sx={{ ...body }}>
              {paragraph}
            </Box>
          ))}
      </Container>
      <Box sx={{ backgroundColor: beige, pt: 8, pb: 8 }}>
        <Container maxWidth="md">
          <Box component="h2" sx={{ ...h2, mb: 2, color: bumpedPurple }}>
            {t("harrassment")}
          </Box>
          <Box component="p" sx={{ ...bodyLarge }}>
            {t("harrassmentIntroOne")}
          </Box>
          <Box component="ul" sx={{ columnCount: 2 }}>
            {t("harrassmentDescriptionOne")
              .split("\n")
              .map((paragraph: string, index: number) => (
                <Box key={index} component="li" sx={{ ...body }}>
                  {paragraph}
                </Box>
              ))}
          </Box>
        </Container>
        <Container maxWidth="md">
          <Box component="p" sx={{ ...h3, fontWeight: 400, padding: 4 }}>
            {t("harrassmentIntroTwo")}
          </Box>
          <Box component="ul">
            {t("harrassmentDescriptionTwo")
              .split("\n")
              .map((paragraph: string, index: number) => (
                <Box key={index} component="li" sx={{ ...body }}>
                  {paragraph}
                </Box>
              ))}
          </Box>
          <Box component="p" sx={{ ...body }}>
            {t("harrassmentDescriptionThree")}
          </Box>
        </Container>
      </Box>
      <Box sx={{ pt: 8, pb: 12 }}>
        <Container maxWidth="md">
          <Box component="h2" sx={{ ...h2, color: bumpedPurple }}>
            {t("reporting")}
          </Box>
          <Box component="p" sx={{ ...body, fontWeight: 700 }}>
            {t("reportingIntro")}
          </Box>
          {t("reportingDescription")
            .split("\n")
            .map((paragraph: string, index: number) => (
              <Box key={index} component="p" sx={{ ...body }}>
                {paragraph}
              </Box>
            ))}
        </Container>
        <Container maxWidth="md">
          <Box component="h2" sx={{ ...h2, color: bumpedPurple, mt: 6 }}>
            {t("consequences")}
          </Box>
          {t("consequencesDescription")
            .split("\n")
            .map((paragraph: string, index: number) => (
              <Box key={index} component="p" sx={{ ...body }}>
                {paragraph}
              </Box>
            ))}
        </Container>
        <Container maxWidth="md">
          <Box component="h2" sx={{ ...h2, color: bumpedPurple, mt: 6 }}>
            {t("licensing")}
          </Box>
          {t("licensingDescription")
            .split("\n")
            .map((paragraph: string, index: number) => (
              <Box key={index} component="p" sx={{ ...body }}>
                {paragraph}
              </Box>
            ))}
        </Container>
      </Box>
    </>
  );
}
