"use client";

import { FC, useState, useEffect } from "react";
import { Container, Grid, Box } from "@mui/material";
import { useSearchParams } from "next/navigation";
import { MiniDivider } from "app/_components/common/MiniDivider";
import { TeamMember } from "./TeamMember";
import { t } from "app/_lib/helpers";
import { colors, typography } from "app/_styles/theme";

interface TeamProps {
  team: any[];
}

export const Team: FC<TeamProps> = ({ team }) => {
  const [selectedTeamMember, setSelectedTeamMember] = useState(null);
  const { almostBlack } = colors;
  const { h2, h3 } = typography;
  const params = useSearchParams();
  
  useEffect(() => {
    const show = params.get("show");
    setSelectedTeamMember(show);
  }, [params]);

  return (
    <Container>
      <Grid container direction="column" spacing={3}>
        <Grid item>
          <a
            href="#team"
            id="team"
            style={{ textDecoration: "none", color: almostBlack }}
          >
            <Box component="h2" sx={{ ...h2, margin: 0 }}>
              {t("meetOurTeam")}
            </Box>
          </a>
        </Grid>
        <MiniDivider />
        <Grid item container direction="column">
          <Grid item>
            <Box
              component="h3"
              sx={{
                ...h3,
                textAlign: "left",
                color: almostBlack,
                marginBottom: 3,
              }}
            >
              {t("staff")}
            </Box>
          </Grid>
          <Grid
            item
            container
            direction="row"
            justifyContent="flex-start"
            alignItems="flex-start"
            rowSpacing={2}
            columnSpacing={1}
          >
            {team
              .filter((member) => member.type === "staff")
              .map((staffMember) => (
                <TeamMember
                  key={staffMember.name}
                  name={staffMember.name}
                  position={staffMember.position}
                  bio={staffMember.bio}
                  emailAddress={staffMember.emailAddress}
                  photoFileName={staffMember.photoFileName}
                  pgpFingerprint={staffMember.pgpFingerprint}
                  selectedTeamMember={selectedTeamMember}
                />
              ))}
          </Grid>
        </Grid>
        <Grid item container direction="column">
          <Grid item>
            <Box
              component="h5"
              sx={{
                ...h3,
                textAlign: "left",
                color: almostBlack,
                marginTop: 3,
                marginBottom: 3,
              }}
            >
              {t("boardMembers")}
            </Box>
          </Grid>
          <Grid item container direction="row">
            {team
              .filter((member) => member.type === "board")
              .map((boardMember) => (
                <TeamMember
                  key={boardMember.name}
                  name={boardMember.name}
                  position={boardMember.position}
                  bio={boardMember.bio}
                  emailAddress={boardMember.emailAddress}
                  photoFileName={boardMember.photoFileName}
                  selectedTeamMember={selectedTeamMember}
                />
              ))}
          </Grid>
        </Grid>
      </Grid>
    </Container>
  );
};
