"use client";

import { FC } from "react";
import { Container, Grid, Box } from "@mui/material";
import { MiniDivider } from "app/_components/common/MiniDivider";
import { t } from "app/_lib/helpers";
import { colors, typography } from "app/_styles/theme";

export const Story: FC = () => {
  const { almostBlack } = colors;
  const { h2, bodyLarge } = typography;

  return (
    <Container maxWidth="md" sx={{ pt: 12, pb: 12 }}>
      <Grid container direction="column" alignItems="center">
        <Grid item>
          <Box
            component="h2"
            sx={{ ...h2, textAlign: "center", color: almostBlack, margin: 0 }}
          >
            {t("ourStory")}
          </Box>
        </Grid>
        <MiniDivider />
        <Grid item container spacing={2}>
          <Grid item>
            <Box
              component="p"
              sx={{ ...bodyLarge, textAlign: "center", margin: 0 }}
            >
              {t("ourStoryIntro")}
            </Box>
          </Grid>
          <Grid item>
            <Box component="p" sx={{ ...bodyLarge, textAlign: "center" }}>
              {t("ourStoryIntroDescription")}
            </Box>
          </Grid>
        </Grid>
      </Grid>
    </Container>
  );
};
