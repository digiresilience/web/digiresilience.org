"use client";

/* eslint-disable global-require */
/* eslint-disable import/no-dynamic-require */
import { FC } from "react";
import Image from "next/image";
import Link from "next/link";
import { Grid, Box, Dialog, DialogContent } from "@mui/material";
import { useRouter } from "next/navigation";
import { typography } from "app/_styles/theme";

interface TeamMemberProps {
  name: string;
  position: string;
  bio: string;
  photoFileName: string;
  emailAddress?: string;
  pgpFingerprint?: string;
  selectedTeamMember?: string;
}

export const TeamMember: FC<TeamMemberProps> = ({
  name,
  position,
  bio,
  photoFileName,
  emailAddress,
  pgpFingerprint,
  selectedTeamMember,
}) => {
  const router = useRouter();
  const { h4, body, bodySmall } = typography;
  const normalizedName = name.trim().replaceAll(" ", "-").toLowerCase();
  let photo: any;
  if (photoFileName) {
    photo = require(`app/_images/dynamic/${photoFileName}`);
  }
  return (
    <Grid
      item
      container
      direction="column"
      justifyContent="center"
      alignItems="center"
      wrap="nowrap"
      xs={12}
      sm={3}
      spacing={0}
      sx={{ cursor: "pointer" }}
    >
      <Grid item sx={{ width: 150, height: 150, textAlign: "center" }}>
        {photoFileName && (
          <Link href={`/about?show=${normalizedName}`} scroll={false}>
            <Image src={photo} alt={name} width={150} height={150} />
          </Link>
        )}
      </Grid>
      <Grid item>
        <Link href={`/about?show=${normalizedName}`} scroll={false}>
          <Box
            component="h4"
            sx={{ ...h4, fontWeight: "bold", margin: 0, marginTop: 1 }}
          >
            {name}
          </Box>
        </Link>
      </Grid>
      <Grid item>
        <Link href={`/about?show=${normalizedName}`} scroll={false}>
          <Box
            component="p"
            sx={{ ...body, textAlign: "center", mt: 0, mb: 2 }}
          >
            {position}
          </Box>
        </Link>
      </Grid>
      <Dialog
        open={selectedTeamMember === normalizedName}
        onClose={() => router.push("/about", { scroll: false })}
      >
        <DialogContent sx={{ p: 4 }}>
          <Grid container direction="column" alignItems="center">
            <Grid item sx={{ width: 150, height: 150, textAlign: "center" }}>
              {photoFileName && (
                <Image src={photo} alt={name} width={150} height={150} />
              )}
            </Grid>
            <Grid item>
              <Box
                component="h4"
                sx={{ ...h4, fontWeight: "bold", margin: 0, marginTop: 1 }}
              >
                {name}
              </Box>
            </Grid>
            <Grid item>
              <Box
                component="p"
                sx={{ ...body, textAlign: "center", mt: 0, mb: 2 }}
              >
                {position}
              </Box>
            </Grid>
            <Grid item>
              {bio.split("\n").map((para, i) => (
                <Box key={i} component="p" sx={{ ...body, mt: 1, mb: 2 }}>
                  {para}
                </Box>
              ))}
            </Grid>
            <Grid
              item
              container
              direction="column"
              justifyContent="space-between"
              wrap="nowrap"
            >
              {emailAddress && (
                <Grid item>
                  <Box component="p" sx={{ ...bodySmall, mt: 2, mb: 1 }}>
                    <Box component="span" sx={{ fontWeight: 700 }}>
                      Email:{` `}
                    </Box>
                    <a href={`mailto:${emailAddress}`}>{emailAddress}</a>
                  </Box>
                </Grid>
              )}
              {pgpFingerprint && (
                <Grid item>
                  <Box component="p" sx={{ ...bodySmall, mb: 2 }}>
                    <Box component="span" sx={{ fontWeight: 700 }}>
                      PGP key:{` `}
                    </Box>
                    <Link href={`/about/keys/${normalizedName}`}>
                      {pgpFingerprint}
                    </Link>
                  </Box>
                </Grid>
              )}
            </Grid>
          </Grid>
        </DialogContent>
      </Dialog>
    </Grid>
  );
};
