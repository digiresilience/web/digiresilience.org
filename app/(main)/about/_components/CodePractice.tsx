"use client";

import { FC } from "react";
import Image from "next/image";
import { Grid, Box } from "@mui/material";
import codePractice from "app/_images/code-practice.png";
import { Button } from "app/_components/common/Button";
import { t } from "app/_lib/helpers";
import { colors, typography } from "app/_styles/theme";

export const CodePractice: FC = () => {
  const { cdrLinkOrange } = colors;
  const { h2, body } = typography;

  return (
    <Grid
      item
      container
      direction="column"
      alignItems="center"
      spacing={0}
      xs={12}
      sm={6}
    >
      <Grid item>
        <Image src={codePractice} alt="" width={100} height={100} />
      </Grid>
      <Grid item>
        <Box component="h2" sx={{ ...h2, margin: 0 }}>
          {t("codeOfPractice")}
        </Box>
      </Grid>
      <Grid item>
        <Box component="p" sx={{ ...body }}>
          {t("codeOfPracticeDescription")}
        </Box>
      </Grid>
      <Grid item>
        <Button
          text="Learn More"
          color={cdrLinkOrange}
          href="/about/code-practice"
        />
      </Grid>
    </Grid>
  );
};
