"use client";

import { FC } from "react";
import Image from "next/image";
import { Container, Grid, Box } from "@mui/material";
import valuePillars from "app/_images/value-pillars.png";
import { Pillar } from "./Pillar";
import { t } from "app/_lib/helpers";
import { typography } from "app/_styles/theme";

export const Approach: FC = () => {
  const { h2, h5 } = typography;

  return (
    <Container maxWidth="md" sx={{ pb: 12 }}>
      <Grid container direction="column" alignItems="center">
        <Grid item>
          <a href="#pillars" id="pillars">
            <Image src={valuePillars} alt="" width={95} height={95} />
          </a>
        </Grid>
        <Grid item>
          <Box component="h5" sx={{ ...h5, margin: 0, mt: 4 }}>
            {t("valuePillars")}
          </Box>
        </Grid>
        <Grid item>
          <Box component="h2" sx={{ ...h2, margin: 0, mb: 4 }}>
            {t("approachWork")}
          </Box>
        </Grid>
        <Grid item container direction="row" columnSpacing={6} rowSpacing={3}>
          <Pillar
            title={t("sustainableEcosystems")}
            description={t("sustainableEcosystemsDescription")}
          />
          <Pillar
            title={t("resilience")}
            description={t("resilienceDescription")}
          />
          <Pillar title={t("trust")} description={t("trustDescription")} />
          <Pillar
            title={t("community")}
            description={t("communityDescription")}
          />
          <Pillar
            title={t("collaboration")}
            description={t("collaborationDescription")}
          />
          <Pillar
            title={t("behaviorChange")}
            description={t("behaviorChangeDescription")}
          />
          <Pillar
            title={t("accessibility")}
            description={t("accessibilityDescription")}
          />
          <Pillar
            title={t("holisticUnderstanding")}
            description={t("holisticUnderstandingDescription")}
          />
          <Pillar
            title={t("peopleBeforeTech")}
            description={t("peopleBeforeTechDescription")}
          />
        </Grid>
      </Grid>
    </Container>
  );
};
