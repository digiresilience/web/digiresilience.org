"use client";

import { FC } from "react";
import Image from "next/image";
import { Container, Grid, Box } from "@mui/material";
import aboutHeader from "app/_images/about-header.png";
import logoLarge from "app/_images/logo-large.svg";
import { t, loader } from "app/_lib/helpers";
import { colors, typography } from "app/_styles/theme";

export const Header: FC = () => {
  const { white } = colors;
  const { h1, h5 } = typography;

  return (
    <Box
      sx={{
        background: `url(${loader(aboutHeader)})`,
        backgroundBlendMode: "burn",
        backgroundPosition: "center",
        backgroundRepeat: "no-repeat",
        backgroundSize: "cover",
        minHeight: "400px",
        padding: 4,
        width: "100%",
      }}
    >
      <Container
        sx={{
          padding: 4,
        }}
      >
        <Grid container direction="row" sx={{ pl: 6, pt: 6, pr: 0, pb: 6 }}>
          <Grid item container direction="column" xs={12} sm={6} sx={{ zIndex: 2}}>
            <Grid item>
              <Box
                component="h5"
                sx={{ ...h5, color: white, textAlign: "left" }}
              >
                {t("buildSecureCommunities")}
              </Box>
            </Grid>
            <Grid item>
              <Box
                component="h1"
                sx={{ ...h1, color: white, textAlign: "left" }}
              >
                {t("civilSocietyThrives")}
              </Box>
            </Grid>
          </Grid>
          <Grid item xs={12} sm={6} sx={{ mt: -10, mb: 0, zIndex: 1 }}>
            <Image
              src={logoLarge}
              alt=""
              style={{ width: "100%", height: "auto" }}
            />
          </Grid>
        </Grid>
      </Container>
    </Box>
  );
};
