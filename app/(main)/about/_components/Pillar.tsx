"use client";

import { FC } from "react";
import { Grid, Box } from "@mui/material";
import { typography } from "app/_styles/theme";

interface PillarProps {
  title: string;
  description: string;
}

export const Pillar: FC<PillarProps> = ({ title, description }) => {
  const { h3, body } = typography;

  return (
    <Grid item container direction="column" xs={12} sm={6}>
      <Grid item>
        <Box
          component="h3"
          sx={{ ...h3, textAlign: "left", margin: 0, marginBottom: 1 }}
        >
          {title}
        </Box>
      </Grid>
      <Grid item>
        <Box component="p" sx={{ ...body, margin: 0 }}>
          {description}
        </Box>
      </Grid>
    </Grid>
  );
};
