import { Metadata } from "next";
import Head from "next/head";
import { Container, Box, Grid } from "@mui/material";
import { TextHeader } from "app/_components/common/TextHeader";
import { CodeConduct } from "../_components/CodeConduct";
import { t } from "app/_lib/helpers";
import { colors, typography } from "app/_styles/theme";

export const metadata: Metadata = {
  title: "CDR: Code of Practice",
};

export default function Page() {
  const {
    beige,
    waterbearLightSmokePurple,
    bumpedPurple,
    neutralNavy,
    almostBlack,
  } = colors;
  const { h2, h3, body } = typography;

  return (
    <>
      <TextHeader text="Code of Practice" />
      <Container maxWidth="md" sx={{ pb: 8 }}>
        <Box component="h2" sx={{ ...h2, color: bumpedPurple }}>
          {t("informationHandlingPolicy")}
        </Box>
        <Box
          component="p"
          sx={{ ...body, fontWeight: 700, textAlign: "center" }}
        >
          {t("informationHandlingPolicyIntro")}
        </Box>
        <Box component="ul">
          {t("informationHandlingPolicyDescription")
            .split("\n")
            .map((paragraph: string, index: number) => (
              <Box key={index} component="li" sx={{ ...body }}>
                {paragraph}
              </Box>
            ))}
        </Box>
      </Container>
      <Box
        sx={{
          backgroundColor: beige,
          pt: 8,
          pb: 8,
        }}
      >
        <Container maxWidth="lg">
          <Container maxWidth="md">
            <Box component="h2" sx={{ ...h2, color: bumpedPurple }}>
              {t("vulnerabilityDisclosurePolicy")}
            </Box>
            <Box
              component="p"
              sx={{ ...body, fontWeight: 700, textAlign: "center" }}
            >
              {t("vulnerabilityDisclosurePolicyIntro")}
            </Box>
            <Box component="ul" sx={{ mb: 12 }}>
              {t("vulnerabilityDisclosurePolicyDescription")
                .split("\n")
                .map((paragraph: string, index: number) => (
                  <Box key={index} component="li" sx={{ ...body }}>
                    {paragraph}
                  </Box>
                ))}
            </Box>
          </Container>
        </Container>
        <Container sx={{ mb: 8 }}>
          <Box component="h3" sx={{ ...h3, color: bumpedPurple, mb: 8 }}>
            {t("disclosureSchedule")}
          </Box>
          <Grid container direction="row" rowSpacing={4} wrap="nowrap">
            <Grid
              item
              xs={12}
              sm={4}
              sx={{
                backgroundColor: waterbearLightSmokePurple,
                padding: 4,
                margin: 2,
                boxShadow: `4px 4px 8px 0px ${almostBlack}50`,
              }}
            >
              <Box
                component="h3"
                sx={{ ...h3, fontWeight: 400, color: neutralNavy }}
              >
                {t("stepOne")}
              </Box>
              <Box
                component="p"
                sx={{ ...body, fontWeight: 700, color: neutralNavy }}
              >
                {t("stepOneDescription")}
              </Box>
            </Grid>
            <Grid
              item
              xs={12}
              sm={4}
              sx={{
                backgroundColor: waterbearLightSmokePurple,
                padding: 4,
                margin: 2,
                boxShadow: `4px 4px 8px 0px ${almostBlack}50`,
              }}
            >
              <Box
                component="h3"
                sx={{ ...h3, fontWeight: 400, color: neutralNavy }}
              >
                {t("stepTwo")}
              </Box>
              <Box
                component="p"
                sx={{ ...body, fontWeight: 700, color: neutralNavy }}
              >
                {t("stepTwoDescription")}
              </Box>
            </Grid>
            <Grid
              item
              xs={12}
              sm={4}
              sx={{
                backgroundColor: waterbearLightSmokePurple,
                padding: 4,
                margin: 2,
                boxShadow: `4px 4px 8px 0px ${almostBlack}50`,
              }}
            >
              <Box
                component="h3"
                sx={{ ...h3, fontWeight: 400, color: neutralNavy }}
              >
                {t("stepThree")}
              </Box>
              <Box
                component="p"
                sx={{ ...body, fontWeight: 700, color: neutralNavy }}
              >
                {t("stepThreeDescription")}
              </Box>
            </Grid>
          </Grid>
        </Container>
        <Container maxWidth="md">
          <Box component="p" sx={{ ...body, fontWeight: 700 }}>
            {t("disclosureScheduleIntro")}
          </Box>
          <ul>
            {t("disclosureScheduleIntroDescription")
              .split("\n")
              .map((paragraph: string, index: number) => (
                <Box key={index} component="li" sx={{ ...body }}>
                  {paragraph}
                </Box>
              ))}
          </ul>
        </Container>
      </Box>
      <Container>
        <Grid
          container
          direction="row"
          justifyContent="space-between"
          sx={{ padding: 12 }}
          spacing={10}
        >
          <Grid item xs={12} sm={6}>
            <Box component="h3" sx={{ ...h3, textAlign: "center", margin: 0 }}>
              {t("memberAgreement")}
            </Box>
            <Box
              component="p"
              sx={{ ...body, fontWeight: 700, textAlign: "center" }}
            >
              {t("memberAgreementDescription")}
            </Box>
          </Grid>
          <Grid item xs={12} sm={6}>
            <Box component="h3" sx={{ ...h3, textAlign: "center", margin: 0 }}>
              {t("partnerAgreement")}
            </Box>
            <Box
              component="p"
              sx={{ ...body, fontWeight: 700, textAlign: "center" }}
            >
              {t("partnerAgreementDescription")}
            </Box>
          </Grid>
        </Grid>
      </Container>
      <Container sx={{ backgroundColor: beige, padding: 4 }}>
        <Grid
          container
          direction="row"
          justifyContent="space-around"
          spacing={4}
        >
          <CodeConduct />
        </Grid>
      </Container>
    </>
  );
}
