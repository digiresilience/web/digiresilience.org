import { promises as fs, existsSync } from "fs";
import { Metadata } from "next";
import path from "path";
import { Grid, Container, Box } from "@mui/material";
import { Client } from "@notionhq/client";
import { Header } from "./_components/Header";
import { Story } from "./_components/Story";
import { Approach } from "./_components/Approach";
import { Team } from "./_components/Team";
import { Network } from "app/_components/common/Network";
import { CodePractice } from "./_components/CodePractice";
import { CodeConduct } from "./_components/CodeConduct";
import { getNotionProperty, retryableRequest } from "app/_lib/notion";
import { colors } from "app/_styles/theme";

export const metadata: Metadata = {
  title: "CDR: About",
};

const getTeam = async () => {
  const notion = new Client({ auth: process.env.NOTION_API_KEY });
  const teamDatabaseID = process.env.NOTION_TEAM_DATABASE_ID;
  const response = await retryableRequest(() => notion.databases.query({
    database_id: teamDatabaseID,
    filter: {
      property: "Active",
      checkbox: {
        equals: true,
      },
    },
    sorts: [
      {
        property: "Type",
        direction: "descending",
      },
      {
        property: "Sort Order",
        direction: "ascending",
      },
    ],
  }));

  const team = await Promise.all(
    response.results.map(async (result: any) => {
      const properties: any = {};
      for await (const key of Object.keys(result.properties)) {
        const val = result.properties[key];
        const prop = await getNotionProperty(notion, result.id, val.id);
        properties[key] = prop;
      }

      const name = properties.Name[0].title.plain_text;
      // @ts-ignore
      const originalPhotoURL = properties.Photo[0].files[0]?.file.url ?? null;
      let photoFileName = null;
      if (originalPhotoURL) {
        const extension = originalPhotoURL.split(".").pop().split("?")[0];
        const fileName = name.trim().replaceAll(" ", "-").toLowerCase();
        photoFileName = `${fileName}.${extension}`;
        const res = await fetch(originalPhotoURL);
        const fullPath = path.resolve(`app/_images/dynamic/${photoFileName}`);
        if (!existsSync(fullPath)) {
          const arrayBuffer = await res.arrayBuffer();
          const buffer = Buffer.from(arrayBuffer);
          await fs.writeFile(fullPath, buffer);
        }
      }

      return {
        id: result.id,
        name,
        position: properties.Position[0].rich_text?.plain_text ?? null,
        emailAddress: properties["Email Address"][0].email ?? null,
        bio: properties.Bio[0].rich_text?.plain_text ?? "",
        pgpKey: properties["PGP Key"][0]?.rich_text?.plain_text ?? null,
        pgpFingerprint:
          properties["PGP Fingerprint"][0]?.rich_text?.plain_text ?? null,
        originalPhotoURL,
        photoFileName,
        type: properties.Type[0]?.select?.name.toLowerCase() ?? null,
      };
    }),
  );

  return team;
};

export default async function Page() {
  const team = await getTeam();
  const { beige } = colors;

  return (
    <Grid container direction="column" spacing={0} alignItems="center">
      <Header />
      <Story />
      <Approach />
      <Team team={team} />
      <Network />
      <Box sx={{ backgroundColor: beige, width: "100%" }}>
        <Container sx={{ padding: 4 }}>
          <Grid
            container
            direction="row"
            justifyContent="space-around"
            alignItems="center"
            spacing={1}
          >
            <CodePractice />
            <CodeConduct />
          </Grid>
        </Container>
      </Box>
    </Grid>
  );
}
