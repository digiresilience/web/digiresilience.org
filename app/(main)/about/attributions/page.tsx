import { Metadata } from "next";
import { Box, Container, Grid } from "@mui/material";
import { TextHeader } from "app/_components/common/TextHeader";
import { t } from "app/_lib/helpers";
import { typography } from "app/_styles/theme";

export const metadata: Metadata = {
  title: "CDR: Attributions",
};

export default function Page() {
  const { h4, body } = typography;

  return (
    <>
      <TextHeader text={t("attributions")} />
      <Container maxWidth="sm" sx={{ pb: 8 }}>
        <Grid container direction="column" spacing={1}>
          <Grid item>
            <Box component="h4" sx={h4}>
              {t("attributionsDescription")}
            </Box>
          </Grid>
          <Grid item>
            <Box component="p" sx={body}>
              <a href="https://www.flickr.com/photos/popicinio/26433687543/">
                Demonstration in Madrid to celebrate the fifth anniversary of
                the birth of 15M / Manifestación en Madrid para celebrar el
                quinto aniversario del nacimiento del 15M
              </a>{" "}
              by{" "}
              <a href="https://www.flickr.com/photos/popicinio/">
                Adolfo Lujan
              </a>{" "}
              is licensed under{" "}
              <a href="https://creativecommons.org/licenses/by-nc-nd/2.0/">
                CC BY-NC-ND 2.0
              </a>
              .
            </Box>
          </Grid>
          <Grid item>
            <Box component="p" sx={body}>
              <a href="https://www.flickr.com/photos/juggernautco/6804732907/in/photolist-bnj32a-28XBezF-9WCcGA-f53VdS-bCh75i-9L7HYj-6R579c-7oKvQL-dEmXw2-bnj2A4-pCqpjs-bxtY7j-Fc8XwM-2akEisF-nDqps5-f541ZL-9VEYdS-2akEiQV-dAR9oH-cJjjsj-9VCb1H-dAR39F-e1ywjT-o2z8rV-VasZea-bChczg-62QNMC-pCk4eX-9L4WyR-oo3NW6-dAQMJF-VasBLa-9VEJty-xsUw9-bpnf7y-pUTF8U-24iNUJf-WjUbW8-6GM12j-aqVBtW-oXYghE-29fdKfq-ea8Sxm-mD4sX-24iNUDA-WYntDi-oh2KtL-4dm8sp-jJ6pZ-o2z7Dx">
                Pilsen Smart Communities Murals
              </a>{" "}
              by{" "}
              <a href="https://www.flickr.com/photos/juggernautco/">
                Daniel X. O&apos;Neil
              </a>{" "}
              is licensed under{" "}
              <a href="https://creativecommons.org/licenses/by/2.0/">
                CC BY 2.0
              </a>
              .
            </Box>
          </Grid>
        </Grid>
      </Container>
    </>
  );
}
