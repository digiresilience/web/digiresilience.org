import { Metadata } from "next";
import { useState } from "react";
import { Container, Box } from "@mui/material";
import { TextHeader } from "app/_components/common/TextHeader";
import { MDXProvider } from "@mdx-js/react";
import { t } from "app/_lib/helpers";
import { colors, typography } from "app/_styles/theme";

### Who We Are

The Center for Digital Resilience (CDR) helps build resilient systems to keep civil society safe online. CDR is headquartered at 1300 I Street NW, Suite 400E, Washington, DC 20005. Please address questions about this Privacy Policy to the Chief Technology Officer at josh@digiresilience.org.

CDR’s mandate is to strengthen the digital resilience of civil society organizations and communities. Through its pillars of Technology, Information and Analysis, and Community Engagement, CDR works with civil society around the world to increase resilience to cyber-attack and disinformation campaigns, providing tools, resources, and data that partners need to help their communities operate safely and effectively online.

### Overview and Scope of our Privacy Policy

CDR is committed to protecting the privacy of visitors to our website, of communities who share their data with us, and of users of CDR’s tech products, including the CDR Link helpdesk and the Waterbear data-gathering platform.

This Privacy Policy explains what data we collect, how it is used, and how it is protected.

This policy applies to all Personal Data and Users who access personal data collected by CDR. All technical and organizational precautions and measures necessary to comply with applicable local laws and regulations, such as the EU General Data Protection Regulation (GDPR), are outlined below.

### Terms and Definitions

**Personal Data** - is information relating to an identified or identifiable natural person.

**User** - means any person or organization that has access by any means to any Personal Data.

**Internal Users** include persons classified as CDR staff, contractors, or contingent workers employed directly by or retained by CDR.

**External Users** include suppliers, business partners, vendors, third party service providers, their employees and their agents who interact with any Personal Data.

**Tech Platforms** - Products developed and maintained by CDR, including the CDR Link helpdesk and Waterbear data-gathering platform.

### Personal Data We Collect

CDR collects your personal data to provide you with our services and products and to meet our legal, statutory, and contractual obligations.

### Website Visitors

Standard web logs are utilized on our websites, but minimized to reduce or remove identifiable information about visitors. We only collect the following data about website visitors:

The date the site was accessed (but not the time or time zone) and Country (by looking up visitors’ IP addresses in a GeoIP database).
Data that is not stored includes referrer, IP addresses, and user agent (information about your browser, operating system, and plugins).

In addition, the CDR websites do not utilize third-party analytics software (like Google Analytics) and do not include beacons or other tracking code. We use session cookies on certain portions of our websites. Session cookies expire when you close your browser.

### Tech Platforms

We collect personal data that a Tech Platform user enters. This may include email addresses, names, phone numbers, addresses, and organization affiliation. The Tech Platform will collect and store IP addresses in both access logs and session logs.

### Communities

CDR stores information shared by or with a partner organization which is necessary to assist and or collaborate with the organization. Wherever possible, CDR practices data minimization, collecting only the personal data required to provide such assistance and associated CDR services and products.

We collect information in the following ways:

- When you send us an email.
- When you submit your information to us via secure services such as Signal.
- When you visit our website.
- When you submit information to us in paper form.
- Through direct interaction, whether in person, by telephone, or online.

### How Personal Data is Used

CDR uses collected Personal Data for specified, explicit and legitimate purposes in the following ways:

- To distribute email notifications and/or newsletters.
- To collaborate with users and partners and provide assistance, services and products for community-building, information-sharing and other specified purposes in accordance with CDR’s mandate and in accordance with applicable laws.
- For internal record keeping.
- To personalize the way CDR content is presented to you.
- To analyze and improve the services and products offered by CDR.

### Tech Platforms

CDR acts as system administrators to manage the software, databases, hosting, archiving, and related security issues. CDR administrators have access to and are able to view all tickets, add/remove users, add/remove groups, and access all data stored in the system. These administrators only access and use information collected to make sure the service is functioning properly; analyze use of the service; and communicate with users (including providing security update emails or messages).

### Who Personal Data is Shared With

CDR will not sell, rent, or release Personal Data to third parties for promotional purposes.

CDR will only use, share or distribute your Personal Data: i) as necessary to maintain the security of our services and products, ii) as required by applicable law, iii) for Tech Platform users, as described in the contract between CDR and the users’ organization, or iv) as otherwise set forth in this Privacy Policy.

### Service providers, subcontractors, agents

We sometimes hire other companies or individuals to perform certain business-related functions. Examples include hosting and/or maintaining databases, translation services, and processing payments. When we employ another party to perform a service or function, we may need to provide them with access to certain Personal Data. In that event, we only provide them with the information that they need to perform their specific service or function. CDR is accountable for any Personal Data that it receives from you and subsequently transfers to these third parties, in accordance with applicable privacy laws. We remain responsible if a third party that we engage to process Personal Data on our behalf does so in a manner inconsistent with applicable law, unless we can prove that we are not responsible for the activities or circumstances giving rise to the claim.

### Access and Control of Personal Data

#### Data Security

CDR shall ensure that Personal Data is stored securely. Access to Personal Data shall be limited to CDR personnel who need access and appropriate security is in place to avoid unauthorized sharing of information. When Personal Data is deleted this shall be done such that the data is irrecoverable.

Tech Platform users are responsible for maintaining the security of their account and the activities on said account.

#### Retention

CDR shall ensure that Personal Data collected are adequate, relevant, and limited to what is necessary in relation to the purposes for which they are processed. CDR adheres to internal data retention policies and procedures and reviews those on a regular basis.

You have the right to delete, correct, amend or update personal data provided to CDR at any time by contacting CDR’s Security Director at anthony@digiresilience.org.

#### Tech Platforms

Tickets and user information are stored until CDR is instructed to delete the data or the retention period lapses. Users have the right to leave the service at any time. Users’ content and activity in CDR Link will remain in the system as dictated by the designated contract.

Log files for the Tech Platform are written to disk. On a nightly basis, log files are archived and old logs are deleted after 14 days.

The Tech Platforms maintain session information about every user currently logged in. This information is automatically deleted when a user logs out, and can be viewed or manually deleted. Users may also delete their own session information via the user preferences menu, under Device. Session information includes IP address (and possibly geographic location), browser, time of original login, and time of last visit.

### Law and Government Requests

CDR reserves the right to disclose Personal Data if we reasonably believe that access, use, preservation or disclosure of such data is necessary to satisfy any applicable law, regulation, legal process, or enforceable government request. CDR is very strict when granting access to such requests and, wherever possible, will inform users of any law or government requests.

### Automated Decisions and Profiling

CDR does not carry out automated decision-making but we systematically analyze our databases and the information that we hold about you, in order to improve the efficiency, and relevance of our communication with you (profiling), and you will be informed about this when we contact you as a result of such profiling by reference to this Privacy Policy.

### Transferring Data Internationally

We may transfer Personal Data outside of the US, UK or EU. We will comply with the laws of the US, UK and the EU in respect to any such transfers. Please be aware that countries outside these jurisdictions may not have the same level of data protection, however, our collection, storage and use of Personal Data will continue to be governed by this Privacy Policy.

### Changes to Our Privacy Policy

At times it may be necessary for us to make changes to this Privacy Policy. Accordingly, we reserve the right to update or modify this Privacy Policy at any time and from time to time without prior notice. We will make an effort to inform all Users of these changes through mailing lists, points of contact, and other designated communication channels.

### Questions and Contact Information

If you have questions or concerns about CDR or our data-handling practices, please contact: josh@digiresilience.org

/*
export const metadata: Metadata = {
  title: "CDR: Privacy Policy",
};

export default async function Page({children}) {
  const { h3, h4, body } = typography;
  
  const [components, setComponents] = useState({
    h3: ({ children }) => (
      <Box component="h3" sx={{ ...h3, textAlign: "left", m: 0, mt: 5, p: 0 }}>
        {children}
      </Box>
    ),
    h4: ({ children }) => (
      <Box component="h4" sx={{ ...h4, textAlign: "left", m: 0, mt: 3 }}>
        {children}
      </Box>
    ),
  });
  return (<>
      <TextHeader text={t("cdrPrivacyPolicy")} />
      <Container maxWidth="md" sx={{ mb: 10 }}>
        <MDXProvider components={components}>{children}</MDXProvider>
      </Container>
    </>
  );
}


*/
