import { Metadata } from "next";
import { Container } from "@mui/material";
import { Client } from "@notionhq/client";
import { retryableRequest } from "app/_lib/notion";
import { Content } from "./_components/Content";

export const metadata: Metadata = {
  title: "CDR: Status",
};

const getStatusInfo = async () => {
  const notion = new Client({ auth: process.env.NOTION_API_KEY });
  const pageID = process.env.NOTION_STATUS_PAGE_ID;
  const blockRequest = await retryableRequest(() => notion.blocks.children.list({
    block_id: pageID,
    page_size: 50,
  }));

  return blockRequest.results;
};

export default async function Page() {
  const statusInfo = await getStatusInfo();
  
  return (
    <Container maxWidth="md" sx={{ mb: 5 }}>
      <Content statusInfo={statusInfo} />
    </Container>
  );
}
