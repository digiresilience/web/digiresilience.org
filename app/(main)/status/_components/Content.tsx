"use client";

import { FC } from "react";
import { Render as RenderNotion } from "@9gustin/react-notion-render";

interface ContentProps {
  statusInfo: any;
}

export const Content: FC<ContentProps> = ({ statusInfo }) => (
 <RenderNotion blocks={statusInfo} classNames />
);
