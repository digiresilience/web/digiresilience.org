import { ReactNode } from "react";
import { Layout as InternalLayout } from "app/_components/common/Layout";

export default function Layout({ children }: { children: ReactNode }) {
  return <InternalLayout>{children}</InternalLayout>;
}
