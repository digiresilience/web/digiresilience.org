import { Metadata } from "next";
import { Header } from "../_components/Header";
import { Intro } from "../_components/Intro";
import { Problem } from "../_components/Problem";
import { Solution } from "../_components/Solution";
import { Outcomes } from "../_components/Outcomes";
import { VerticalCallout } from "app/_components/common/VerticalCallout";
import { HorizontalCallout } from "app/_components/common/HorizontalCallout";
import safeSpaceHeader from "app/_images/safe-space-header.png";
import knowledgeGap from "app/_images/knowledge-gap.png";
import hiddenInformation from "app/_images/hidden-information.png";
import oneOffTrainings from "app/_images/one-off-trainings.png";
import resiliencyTools from "app/_images/resiliency-tools.png";
import incidentResponse from "app/_images/incident-response.png";
import wellnessPlans from "app/_images/wellness-plans.png";
import threatAwareness from "app/_images/threat-awareness.png";
import safeSpaceSmall from "app/_images/safe-space-small.png";
import shiftPrevention from "app/_images/shift-prevention.png";
import structuresResilience from "app/_images/structures-resilience.png";
import { t } from "app/_lib/helpers";
import { colors } from "app/_styles/theme";

export const metadata: Metadata = {
  title: "CDR: MENA Case Study",
};

export default async function Page() {
  const { bumpedPurple } = colors;

  return (
    <>
      <Header
        title={t("creatingSafeSpace")}
        subtitle={t("digitalSecurityProject")}
        image={safeSpaceHeader}
        background={bumpedPurple}
      />
      <Intro
        introduction={t("creatingSafeSpaceIntro")}
        description={t("creatingSafeSpaceIntroDescription")}
      />
      <Problem>
        <VerticalCallout
          title={t("knowledgeGap")}
          description={t("knowledgeGapDescription")}
          image={knowledgeGap}
        />
        <VerticalCallout
          title={t("hiddenInformation")}
          description={t("hiddenInformationDescription")}
          image={hiddenInformation}
        />
        <VerticalCallout
          title={t("oneOffTrainings")}
          description={t("oneOffTrainingsDescription")}
          image={oneOffTrainings}
        />
      </Problem>
      <Solution>
        <HorizontalCallout
          title={t("resiliencyToolsProcesses")}
          description={t("resiliencyToolsProcessesDescription")}
          image={resiliencyTools}
        />
        <HorizontalCallout
          title={t("incidentResponseSupport")}
          description={t("incidentResponseSupportDescription")}
          image={incidentResponse}
        />
        <HorizontalCallout
          title={t("digitalWellnessPlans")}
          description={t("digitalWellnessPlansDescription")}
          image={wellnessPlans}
        />
        <HorizontalCallout
          title={t("threatAwareness")}
          description={t("threatAwarenessDescription")}
          image={threatAwareness}
        />
      </Solution>
      <Outcomes>
        <VerticalCallout
          title={t("safeSpace")}
          description={t("safeSpaceDescription")}
          image={safeSpaceSmall}
        />
        <VerticalCallout
          title={t("shiftToPrevention")}
          description={t("shiftToPreventionDescription")}
          image={shiftPrevention}
        />
        <VerticalCallout
          title={t("structuresForResilience")}
          description={t("structuresForResilienceDescription")}
          image={structuresResilience}
        />
      </Outcomes>
    </>
  );
}
