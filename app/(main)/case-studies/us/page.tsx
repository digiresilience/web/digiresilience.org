import { Header } from "../_components/Header";
import { Intro } from "../_components/Intro";
import { Solution } from "../_components/Solution";
import { Outcomes } from "../_components/Outcomes";
import { Problem } from "../_components/Problem";
import forTruthHeader from "app/_images/for-truth-header.png";
import { VerticalCallout } from "app/_components/common/VerticalCallout";
import { HorizontalCallout } from "app/_components/common/HorizontalCallout";
import limitedData from "app/_images/limited-data.png";
import complicatedProcesses from "app/_images/complicated-processes.png";
import inaccessibleData from "app/_images/inaccessible-data.png";
import disinformationTools from "app/_images/disinformation-tools.png";
import easyAnalyze from "app/_images/easy-analyze.png";
import crowdsourcingDisinfo from "app/_images/crowdsourcing-disinfo.png";
import dataAccessible from "app/_images/data-accessible.png";
import democratizedData from "app/_images/democratized-data.png";
import tacticsExposed from "app/_images/tactics-exposed.png";
import smarterResponse from "app/_images/smarter-response.png";
import { t } from "app/_lib/helpers";
import { colors } from "app/_styles/theme";

export default async function Page() {
  const { leafcutterElectricBlue } = colors;

  return (
    <>
      <Header
        title={t("fightingForTruth")}
        subtitle={t("disinformationProject")}
        image={forTruthHeader}
        background={leafcutterElectricBlue}
      />
      <Intro
        introduction={t("fightingForTruthIntro")}
        description={t("fightingForTruthIntroDescription")}
      />
      <Problem>
        <VerticalCallout
          title={t("limitedData")}
          description={t("limitedDataDescription")}
          image={limitedData}
        />
        <VerticalCallout
          title={t("complicatedProcesses")}
          description={t("complicatedProcessesDescription")}
          image={complicatedProcesses}
        />
        <VerticalCallout
          title={t("inaccessibleData")}
          description={t("inaccessibleDataDescription")}
          image={inaccessibleData}
        />
      </Problem>
      <Solution>
        <HorizontalCallout
          title={t("disinformationToolsProcesses")}
          description={t("disinformationToolsProcessesDescription")}
          image={disinformationTools}
        />
        <HorizontalCallout
          title={t("makingDataEasy")}
          description={t("makingDataEasyDescription")}
          image={easyAnalyze}
        />
        <HorizontalCallout
          title={t("crowdsourcingDisinformation")}
          description={t("crowdsourcingDisinformationDescription")}
          image={crowdsourcingDisinfo}
        />
        <HorizontalCallout
          title={t("makingDataAccessible")}
          description={t("makingDataAccessibleDescription")}
          image={dataAccessible}
        />
      </Solution>
      <Outcomes>
        <VerticalCallout
          title={t("democratizedData")}
          description={t("democratizedDataDescription")}
          image={democratizedData}
        />
        <VerticalCallout
          title={t("tacticsExposed")}
          description={t("tacticsExposedDescription")}
          image={tacticsExposed}
        />
        <VerticalCallout
          title={t("smarterResponse")}
          description={t("smarterResponseDescription")}
          image={smarterResponse}
        />
      </Outcomes>
    </>
  );
}
