import { Metadata } from "next";
import { Box, Container } from "@mui/material";
import { TextHeader } from "app/_components/common/TextHeader";
import { Project } from "./_components/Project";
import { Network } from "app/_components/common/Network";
import safeSpaceLarge from "app/_images/safe-space-large.png";
import forTruthLarge from "app/_images/for-truth-large.png";
import { t } from "app/_lib/helpers";
import { colors } from "app/_styles/theme";

export const metadata: Metadata = {
  title: "CDR: Case Studies",
};

export default async function Page() {
  const { beige } = colors;

  return (
    <>
      <TextHeader text={t("caseStudies")} />
      <Project
        title={t("creatingSafeSpace")}
        subtitle={t("digitalSecurityProject")}
        description={t("creatingSafeSpaceDescription")}
        image={safeSpaceLarge}
        direction="row"
        href="/case-studies/mena"
      />
      <Box sx={{ margin: 0, mt: 6 }}>
        <Box
          sx={{
            width: "100%",
            padding: 4,
            paddingTop: 18,
            clipPath: "polygon(0% 0%, 100% 13%, 100% 100%, 0% 100%)",
            background: beige,
          }}
        >
          <Container>
            <Box
              sx={{
                width: "100%",
              }}
            >
              <Project
                title={t("fightingForTruth")}
                subtitle={t("disinformationProject")}
                description={t("fightingForTruthDescription")}
                image={forTruthLarge}
                direction="row-reverse"
                href="/case-studies/us"
              />
            </Box>
          </Container>
        </Box>
      </Box>
      <Network />
    </>
  );
}
