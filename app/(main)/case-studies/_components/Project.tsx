"use client";

import { FC } from "react";
import Image, { StaticImageData } from "next/image";
import { Container, Grid, Box } from "@mui/material";
import { Button } from "app/_components/common/Button";
import { colors, typography } from "app/_styles/theme";

interface ProjectProps {
  title: string;
  subtitle: string;
  description: string;
  // eslint-disable-next-line no-undef
  image: StaticImageData;
  href: string;
  direction: "row" | "row-reverse";
}

export const Project: FC<ProjectProps> = ({
  title,
  subtitle,
  description,
  image,
  href,
  direction = "row",
}) => {
  const { cdrLinkOrange } = colors;
  const { h3, h5, body } = typography;

  return (
    <Container>
      <Grid container direction={direction} spacing={4}>
        <Grid item xs={12} sm={6}>
          <Grid container direction="column">
            <Grid item>
              <Box component="h5" sx={{ ...h5, textAlign: "left" }}>
                {subtitle}
              </Box>
            </Grid>
            <Grid item>
              <Box component="h3" sx={{ ...h3, textAlign: "left" }}>
                {title}
              </Box>
            </Grid>
            <Grid item>
              <Box component="p" sx={{ ...body }}>
                {description}
              </Box>
            </Grid>
            <Grid item>
              <Button text="View Project" color={cdrLinkOrange} href={href} />
            </Grid>
          </Grid>
        </Grid>
        <Grid item xs={12} sm={6} sx={{ maxWidth: 600 }}>
          <Image src={image} alt="" style={{ width: "100%", height: "auto" }} />
        </Grid>
      </Grid>
    </Container>
  );
};
