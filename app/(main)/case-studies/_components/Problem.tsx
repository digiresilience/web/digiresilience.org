"use client";

import { FC, PropsWithChildren } from "react";
import { Container, Grid, Box } from "@mui/material";
import { t } from "app/_lib/helpers";
import { typography } from "app/_styles/theme";

export const Problem: FC<PropsWithChildren> = ({ children }) => {
  const { h2 } = typography;

  return (
    <Container sx={{ mt: 8 }}>
      <Grid container direction="column">
        <Grid item>
          <Box component="h2" sx={{ ...h2, mb: 4 }}>
            {t("theProblem")}
          </Box>
        </Grid>
        <Grid
          item
          container
          direction="row"
          justifyContent="space-between"
          spacing={4}
        >
          {children}
        </Grid>
      </Grid>
    </Container>
  );
};
