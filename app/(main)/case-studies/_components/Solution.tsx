"use client";

import { FC, PropsWithChildren } from "react";
import { Container, Grid, Box } from "@mui/material";
import { t } from "app/_lib/helpers";
import { colors, typography } from "app/_styles/theme";

export const Solution: FC<PropsWithChildren> = ({ children }) => {
  const { beige } = colors;
  const { h2 } = typography;

  return (
    <Box sx={{ margin: 0, mt: 6 }}>
      <Box
        sx={{
          width: "100%",
          padding: 4,
          paddingTop: 18,
          clipPath: "polygon(0 17%, 100% 0, 100% 100%, 0% 100%)",
          background: beige,
        }}
      >
        <Box
          sx={{
            width: "100%",
          }}
        >
          <Container sx={{ pb: 2 }}>
            <Grid container direction="column">
              <Grid item>
                <Box component="h2" sx={{ ...h2, mb: 4 }}>
                  {t("ourSolution")}
                </Box>
              </Grid>
              <Grid
                item
                container
                direction="row"
                justifyContent="space-between"
                spacing={4}
              >
                {children}
              </Grid>
            </Grid>
          </Container>
        </Box>
      </Box>
    </Box>
  );
};
