"use client";

import { FC } from "react";
import { Container, Grid, Box } from "@mui/material";
import { colors, typography } from "app/_styles/theme";

interface IntroProps {
  introduction: string;
  description: string;
}

export const Intro: FC<IntroProps> = ({ introduction, description }) => {
  const { bumpedPurple } = colors;
  const { h2, h3 } = typography;

  return (
    <Container maxWidth="md" sx={{ pt: 4 }}>
      <Grid container direction="column">
        <Grid item>
          <Box component="h2" sx={{ ...h2, color: bumpedPurple, mt: 4, mb: 2 }}>
            {introduction}
          </Box>
        </Grid>
        <Grid item>
          <Box component="h3" sx={{ ...h3, fontWeight: "normal", mb: 4 }}>
            {description}
          </Box>
        </Grid>
      </Grid>
    </Container>
  );
};
