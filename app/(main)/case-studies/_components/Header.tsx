"use client";

import { FC } from "react";
import { StaticImageData } from "next/image";
import { Container, Grid, Box } from "@mui/material";
import { loader } from "app/_lib/helpers";
import { colors, typography } from "app/_styles/theme";

interface HeaderProps {
  title: string;
  subtitle: string;
  background: string;
  // eslint-disable-next-line no-undef
  image: StaticImageData;
}

export const Header: FC<HeaderProps> = ({
  title,
  subtitle,
  background,
  image,
}) => {
  const { white } = colors;
  const { h1, h5 } = typography;

  return (
    <Box
      sx={{
        background: `linear-gradient(0deg, ${background}90, ${background}90), url(${loader(
          image,
        )})`,
        backgroundBlendMode: "lighten",
        backgroundPosition: "center",
        backgroundRepeat: "no-repeat",
        backgroundSize: "cover",
        minHeight: "400px",
      }}
    >
      <Container>
        <Grid
          container
          direction="column"
          justifyContent="space-around"
          sx={{ minHeight: "400px" }}
        >
          <Grid item sx={{ ml: 6 }}>
            <Box
              component="h5"
              sx={{
                ...h5,
                color: white,
                textAlign: "left",
                textTransform: "uppercase",
              }}
            >
              {subtitle}
            </Box>
            <Box component="h1" sx={{ ...h1, color: white, textAlign: "left" }}>
              {title}
            </Box>
          </Grid>
        </Grid>
      </Container>
    </Box>
  );
};
