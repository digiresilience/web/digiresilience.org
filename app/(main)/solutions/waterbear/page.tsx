import { Metadata } from "next";
import { Help } from "../_components/Help";
import { MoreSolutions } from "../_components/MoreSolutions";
import { Header } from "../_components/Header";
import { Feature } from "../_components/Feature";
import { SolutionCallout } from "app/_components/common/SolutionCallout";
import waterbearHeader from "app/_images/waterbear-header.png";
import linkLogoSmall from "app/_images/link-logo-small.png";
import waterbearLogoSmall from "app/_images/waterbear-logo-small.png";
import dwcLogo from "app/_images/dwc-logo.png";
import collectingSecure from "app/_images/collecting-secure.png";
import organizeAnalyze from "app/_images/organize-analyze.png";
import cleanupDisinfo from "app/_images/cleanup-disinfo.png";
import shareTrusted from "app/_images/share-trusted.png";
import { colors } from "app/_styles/theme";
import { t } from "app/_lib/helpers";

export const metadata: Metadata = {
  title: "CDR: Waterbear",
};

export default function Page() {
  const {
    cdrLinkOrange,
    dwcDarkBlue,
    waterbearElectricPurple,
    waterbearLightSmokePurple,
    white,
  } = colors;

  return (
    <>
      <Header
        appName={t("waterbear")}
        title={t("crowdsourceDisinfo")}
        logo={waterbearLogoSmall}
        description={t("crowdsourceDisinfoDescription")}
        color={waterbearElectricPurple}
        backgroundColor={waterbearLightSmokePurple}
        image={waterbearHeader}
      />
      <Help />
      <Feature
        title={t("collectingInfo")}
        subtitle={t("input")}
        description={t("collectingInfoDescription")}
        backgroundColor={white}
        image={collectingSecure}
        direction="row"
      />
      <Feature
        title={t("organizeAnalyze")}
        subtitle={t("cdrLink")}
        description={t("organizeAnalyzeDescription")}
        backgroundColor={waterbearLightSmokePurple}
        image={organizeAnalyze}
        direction="row"
      />
      <Feature
        title={t("cleanUpDisinfo")}
        subtitle={t("qaTool")}
        description={t("cleanUpDisinfoDescription")}
        backgroundColor={white}
        image={cleanupDisinfo}
        direction="row"
      />
      <Feature
        title={t("shareTrustedNetworks")}
        subtitle={t("data")}
        description={t("shareTrustedNetworksDescription")}
        backgroundColor={waterbearLightSmokePurple}
        image={shareTrusted}
        direction="row"
      />
      <MoreSolutions>
        <SolutionCallout
          name={t("cdrLink")}
          description={t("cdrLinkDescription")}
          image={linkLogoSmall}
          href="/solutions/link"
          color={cdrLinkOrange}
          height={300}
        />
        <SolutionCallout
          name={t("wellnessCheck")}
          description={t("wellnessCheckDescription")}
          image={dwcLogo}
          href="/solutions/wellness-check"
          color={dwcDarkBlue}
          height={300}
        />
      </MoreSolutions>
    </>
  );
}
