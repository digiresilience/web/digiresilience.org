import { Metadata } from "next";
import { TextHeader } from "app/_components/common/TextHeader";
import { colors } from "app/_styles/theme";
import { t } from "app/_lib/helpers";
import { Solutions as SolutionsList } from "./_components/Solutions";

export const metadata: Metadata = {
  title: "CDR: Tech Solutions",
};

export default async function Page() {
  const { beige } = colors;

  return (
    <>
      <TextHeader text={t("techSolutions")} backgroundColor={beige} />
      <SolutionsList />
    </>
  );
}
