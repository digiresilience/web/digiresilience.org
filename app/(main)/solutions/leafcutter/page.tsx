import { Metadata } from "next";
import { Help } from "../_components/Help";
import { MoreSolutions } from "../_components/MoreSolutions";
import { Header } from "../_components/Header";
import { Feature } from "../_components/Feature";
import { SolutionCallout } from "app/_components/common/SolutionCallout";
import linkLogoSmall from "app/_images/link-logo-small.png";
import waterbearLogoSmall from "app/_images/waterbear-logo-small.png";
import leafcutterLogo from "app/_images/leafcutter-logo.png";
import lc1 from "app/_images/lc-banner-1.png";
import lc2 from "app/_images/lc-banner-2.png";
import lc3 from "app/_images/lc-banner-3.png";
import lc4 from "app/_images/lc-banner-4.png";
import { colors } from "app/_styles/theme";
import { t } from "app/_lib/helpers";

export const metadata: Metadata = {
  title: "CDR: Leafcutter",
};

export default function Page() {
  const {
    cdrLinkOrange,
    white,
    leafcutterElectricBlue,
    leafcutterLightBlue,
    waterbearElectricPurple,
  } = colors;

  return (
    <>
      <Header
        appName={t("leafcutter")}
        title={undefined}
        description={t("leafcutterDescription")}
        color={leafcutterElectricBlue}
        backgroundColor={leafcutterLightBlue}
        logo={leafcutterLogo}
        image={lc1}
        direction="row-reverse"
      />
      <Help />
      <Feature
        title={t("dataAnalysis")}
        subtitle={undefined}
        description={t("dataAnalysisDescription")}
        backgroundColor={white}
        image={lc2}
        direction="row"
      />
      <Feature
        title={t("createSaveShare")}
        subtitle={undefined}
        description={t("createSaveShareDescription")}
        backgroundColor={leafcutterLightBlue}
        image={lc3}
        direction="row"
      />
      <Feature
        title={t("strengthenCommunity")}
        subtitle={undefined}
        description={t("strengthenCommunityDescription")}
        backgroundColor={white}
        image={lc4}
        direction="row"
      />
      <MoreSolutions>
        <SolutionCallout
          name={t("cdrLink")}
          description={t("cdrLinkDescription")}
          image={linkLogoSmall}
          href="/solutions/link"
          color={cdrLinkOrange}
          height={300}
        />
        <SolutionCallout
          name={t("waterbear")}
          description={t("waterbearDescription")}
          image={waterbearLogoSmall}
          href="/solutions/waterbear"
          color={waterbearElectricPurple}
          height={300}
        />
      </MoreSolutions>
    </>
  );
}
