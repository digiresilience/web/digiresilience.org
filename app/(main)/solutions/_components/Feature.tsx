import { FC } from "react";
import Image, { StaticImageData } from "next/image";
import { Container, Grid, Box } from "@mui/material";
import { Button } from "app/_components/common/Button";
import { colors, typography } from "app/_styles/theme";
import { t } from "app/_lib/helpers";

interface FeatureProps {
  title: string;
  subtitle?: string;
  description: string;
  direction: "row" | "row-reverse";
  backgroundColor: string;
  image: StaticImageData;
}

export const Feature: FC<FeatureProps> = ({
  title,
  subtitle,
  description,
  backgroundColor,
  direction = "row",
  image,
}) => {
  const { cdrLinkOrange } = colors;
  const { h3, h5, body } = typography;

  return (
    <Box sx={{ backgroundColor, pt: 6, pb: 6 }}>
      <Container>
        <Grid container direction={direction} spacing={4} alignItems="center">
          <Grid item xs={12} sm={6} sx={{ padding: 4, maxWidth: 500 }}>
            <Image
              src={image}
              alt=""
              style={{ width: "100%", height: "auto" }}
            />
          </Grid>
          <Grid item xs={12} sm={6}>
            <Grid item container direction="column">
              <Grid item>
                {subtitle && (
                  <Box component="h5" sx={{ ...h5, textAlign: "left" }}>
                    {subtitle}
                  </Box>
                )}
              </Grid>
              <Grid item>
                <Box component="h3" sx={{ ...h3, textAlign: "left" }}>
                  {title}
                </Box>
              </Grid>
              <Grid item>
                <Box
                  component="p"
                  sx={{ ...body, fontWeight: 400, textAlign: "left" }}
                >
                  {description.split("\n").map((line, i) => (
                    <Box key={i}>
                      {line}
                      <br />
                    </Box>
                  ))}
                </Box>
              </Grid>
              <Grid item>
                <Button
                  text={t("requestADemo")}
                  color={cdrLinkOrange}
                  href="mailto:info@digiresilience.org"
                />
              </Grid>
            </Grid>
          </Grid>
        </Grid>
      </Container>
    </Box>
  );
};
