import { FC } from "react";
import { Grid } from "@mui/material";
import cdrLinkSolution from "app/_images/cdr-link-solution.png";
import waterbearSolution from "app/_images/waterbear-solution.png";
import dwcSolution from "app/_images/dwc-solution.png";
import leafcutterSolution from "app/_images/lc-banner-1.png";
import { colors } from "app/_styles/theme";
import { t } from "app/_lib/helpers";
import { Solution } from "./Solution";

export const Solutions: FC = () => {
  const {
    cdrLinkOrange,
    beige,
    waterbearElectricPurple,
    dwcDarkBlue,
    dwcLightGray,
    leafcutterElectricBlue,
    white,
  } = colors;

  return (
    <Grid container direction="column" spacing={12} sx={{ pb: 6 }}>
      <Solution
        name={t("cdrLink")}
        description={t("cdrLinkDescription")}
        href="/solutions/link"
        color={cdrLinkOrange}
        background={beige}
        image={cdrLinkSolution}
        direction="row"
      />
      <Solution
        name={t("leafcutter")}
        description={t("leafcutterDescription")}
        href="/solutions/leafcutter"
        color={leafcutterElectricBlue}
        background={white}
        image={leafcutterSolution}
        direction="row-reverse"
      />
      <Solution
        name={t("wellnessCheck")}
        description={t("wellnessCheckDescription")}
        href="/solutions/wellness-check"
        color={dwcDarkBlue}
        background={dwcLightGray}
        image={dwcSolution}
        direction="row"
      />
      <Solution
        name={t("waterbear")}
        description={t("waterbearDescription")}
        href="/solutions/waterbear"
        color={waterbearElectricPurple}
        background={white}
        image={waterbearSolution}
        direction="row-reverse"
      />
    </Grid>
  );
};
