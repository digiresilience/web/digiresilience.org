import { FC, PropsWithChildren } from "react";
import { Container, Grid, Box } from "@mui/material";
import { MiniDivider } from "app/_components/common/MiniDivider";
import { typography } from "app/_styles/theme";
import { t } from "app/_lib/helpers";

export const MoreSolutions: FC<PropsWithChildren> = ({ children }) => {
  const { h2 } = typography;

  return (
    <Container sx={{ pt: 6, pb: 6 }}>
      <Grid container direction="column">
        <Grid item>
          <Box component="h2" style={h2}>
            {t("interestedInMore")}
          </Box>
        </Grid>
        <MiniDivider />
        <Grid container direction="row">
          {children}
        </Grid>
      </Grid>
    </Container>
  );
};
