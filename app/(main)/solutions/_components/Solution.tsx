import { FC } from "react";
import Image, { StaticImageData } from "next/image";
import { Grid, Box, Container } from "@mui/material";
import { Button } from "app/_components/common/Button";
import { typography } from "app/_styles/theme";

interface SolutionProps {
  name: string;
  description: string;
  href: string | undefined;
  color: string;
  background: string;
  direction: any;
  image: StaticImageData;
}

export const Solution: FC<SolutionProps> = ({
  name,
  description,
  href,
  color,
  background,
  direction,
  image,
}) => {
  const { h3, body } = typography;

  return (
    <Grid item sx={{ backgroundColor: background }}>
      <Box sx={{ mb: 3 }}>
        <Container>
          <Grid
            item
            container
            wrap="nowrap"
            sx={{ backgroundColor: background, padding: 0, margin: 0, flexDirection: {
              xs: "column-reverse",
              sm: direction
            } }}
          >
            <Grid
              item
              container
              direction="column"
              alignItems="flex-start"
              justifyContent="center"
              wrap="nowrap"
              xs={12}
              sm={6}
              sx={{ backgroundColor: background, padding: 0, margin: 0 }}
            >
              <Grid item>
                <Box component="h3" sx={{ ...h3 }}>
                  {name}
                </Box>
              </Grid>
              <Grid item>
                <Box component="p" sx={{ ...body }}>
                  {description}
                </Box>
              </Grid>
              {href && <Button text="Learn More" color={color} href={href} />}
            </Grid>
            <Grid item xs={12} sm={6}>
              <Box
                sx={{
                  maxWidth: "90%",
                  margin: "0 auto",
                }}
              >
                <Image src={image} alt="" style={{width: "100%", height: "auto"}}  />
              </Box>
            </Grid>
          </Grid>
        </Container>
      </Box>
    </Grid>
  );
};
