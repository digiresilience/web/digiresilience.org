"use client";

import { FC } from "react";
import Image, { StaticImageData } from "next/image";
import { Container, Grid, Box } from "@mui/material";
import { Button } from "app/_components/common/Button";
import { typography } from "app/_styles/theme";

interface HeaderProps {
  appName: string;
  title: string;
  description: string;
  color: string;
  backgroundColor: string;
  logo: StaticImageData;
  image: StaticImageData;
  direction?: "row" | "row-reverse";
}

export const Header: FC<HeaderProps> = ({
  appName,
  title,
  description,
  color,
  backgroundColor,
  logo,
  image,
  direction = "row",
}) => {
  const { h1, h2, body } = typography;

  return (
    <Box sx={{ backgroundColor }}>
      <Container sx={{ padding: 4 }}>
        <Grid container direction={direction}>
          <Grid item container direction="column" xs={12} sm={6} sx={{ px: 4 }}>
            <Grid item sx={{ mb: 2 }}>
              <Image src={logo} alt="" width={100} height={100} />
            </Grid>
            <Grid item>
              <Box
                component="h2"
                sx={{ ...h2, color, textAlign: "left", margin: 0 }}
              >
                {appName}
              </Box>
            </Grid>
            <Grid item>
              <Box component="h1" sx={{ ...h1, textAlign: "left" }}>
                {title}
              </Box>
            </Grid>
            <Grid item>
              <Box component="p" sx={{ ...body }}>
                {description}
              </Box>
            </Grid>
            <Grid item>
              <Button
                text="Request a Demo"
                color={color}
                href="mailto:info@digiresilience.org"
              />
            </Grid>
          </Grid>
          <Grid item xs={12} sm={6} sx={{ maxWidth: 500 }}>
            <Image
              src={image}
              alt={title}
              style={{ width: "100%", height: "auto" }}
            />
          </Grid>
        </Grid>
      </Container>
    </Box>
  );
};
