import { FC } from "react";
import { Container, Grid, Box } from "@mui/material";
import Link from "next/link";
import { colors, typography } from "app/_styles/theme";

export const Help: FC = () => {
  const { body } = typography;
  const { helpYellow } = colors;

  return (
    <Box sx={{ backgroundColor: helpYellow }}>
      <Container sx={{ padding: 0, mt: 0 }}>
        <Grid
          container
          direction="column"
          justifyContent="center"
          sx={{ pt: 1, pb: 1 }}
        >
          <Grid
            item
            container
            direction="row"
            spacing={1}
            justifyContent="center"
            alignContent="center"
            wrap="nowrap"
          >
            <Grid item>
              <Box component="p" sx={{ ...body }}>
                👉
              </Box>
            </Grid>
            <Grid item>
              <Box component="p" sx={{ ...body }}>
                Need help with our solutions?
              </Box>
            </Grid>
            <Grid item>
              <Box component="p" sx={{ ...body }}>
                <Link href="https://docs.digiresilience.org">
                  Visit our documentation site.
                </Link>
              </Box>
            </Grid>
          </Grid>
        </Grid>
      </Container>
    </Box>
  );
};
