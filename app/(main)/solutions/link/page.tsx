import { Metadata } from "next";
import { Help } from "../_components/Help";
import { MoreSolutions } from "../_components/MoreSolutions";
import { Header } from "../_components/Header";
import { Feature } from "../_components/Feature";
import { SolutionCallout } from "app/_components/common/SolutionCallout";
import linkHeader from "app/_images/link-header.png";
import simplifiedStatusTracking from "app/_images/simplified-status-tracking.png";
import communityImprove from "app/_images/community-improve.png";
import linkLogoSmall from "app/_images/link-logo-small.png";
import dwcLogo from "app/_images/dwc-logo.png";
import leafcutterLogo from "app/_images/leafcutter-logo.png";
import { colors } from "app/_styles/theme";
import { t } from "app/_lib/helpers";

export const metadata: Metadata = {
  title: "CDR: Link",
};

export default function Page() {
  const { leafcutterElectricBlue, dwcDarkBlue, beige, white, cdrLinkOrange } =
    colors;

  return (
    <>
      <Header
        appName={t("cdrLink")}
        title={t("secureHelpdesk")}
        description={t("secureHelpdeskDescription")}
        color={cdrLinkOrange}
        backgroundColor={beige}
        logo={linkLogoSmall}
        image={linkHeader}
      />
      <Help />
      <Feature
        title={t("simplifiedStatusTracking")}
        description={t("simplifiedStatusTrackingDescription")}
        direction="row-reverse"
        backgroundColor={white}
        image={simplifiedStatusTracking}
      />
      <Feature
        title={t("seeCommunityImprove")}
        description={t("seeCommunityImproveDescription")}
        direction="row"
        backgroundColor={beige}
        image={communityImprove}
      />
      <MoreSolutions>
        <SolutionCallout
          name={t("leafcutter")}
          description={t("leafcutterDescription")}
          image={leafcutterLogo}
          href="/solutions/leafcutter"
          color={leafcutterElectricBlue}
          height={300}
        />
        <SolutionCallout
          name={t("wellnessCheck")}
          description={t("wellnessCheckDescription")}
          image={dwcLogo}
          href="/solutions/wellness-check"
          color={dwcDarkBlue}
          height={300}
        />
      </MoreSolutions>
    </>
  );
}
