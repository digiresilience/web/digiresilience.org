import { Metadata } from "next";
import { Help } from "../_components/Help";
import { MoreSolutions } from "../_components/MoreSolutions";
import { Header } from "../_components/Header";
import { Feature } from "../_components/Feature";
import { SolutionCallout } from "app/_components/common/SolutionCallout";
import linkLogoSmall from "app/_images/link-logo-small.png";
import leafcutterLogo from "app/_images/leafcutter-logo.png";
import wellnessCheckLogo from "app/_images/dwc-logo.png";
import wellnessCheckHeader from "app/_images/wellness-check-header.png";
import dwc1 from "app/_images/dwc-1.png";
import dwc2 from "app/_images/dwc-2.png";
import dwc3 from "app/_images/dwc-3.png";
import dwc4 from "app/_images/dwc-4.png";
import { colors } from "app/_styles/theme";
import { t } from "app/_lib/helpers";

export const metadata: Metadata = {
  title: "CDR: Digital Wellness Check",
};

export default function Page() {
  const {
    cdrLinkOrange,
    white,
    leafcutterElectricBlue,
    waterbearLightSmokePurple,
    dwcDarkBlue,
  } = colors;

  return (
    <>
      <Header
        appName={t("wellnessCheck")}
        title={t("riskAssessmentsBeautiful")}
        description={t("riskAssessmentsBeautifulDescription")}
        color={dwcDarkBlue}
        backgroundColor={waterbearLightSmokePurple}
        logo={wellnessCheckLogo}
        image={wellnessCheckHeader}
      />
      <Help />
      <Feature
        title={t("holisticAssessment")}
        subtitle={t("intake")}
        description={t("holisticAssessmentDescription")}
        backgroundColor={white}
        image={dwc1}
        direction="row"
      />
      <Feature
        title={t("deliverReport")}
        subtitle={t("generate")}
        description={t("deliverReportDescription")}
        backgroundColor={waterbearLightSmokePurple}
        image={dwc2}
        direction="row"
      />
      <Feature
        title={t("generatePDF")}
        subtitle={t("share")}
        description={t("generatePDFDescription")}
        backgroundColor={white}
        image={dwc3}
        direction="row"
      />
      <Feature
        title={t("checkBackUpdate")}
        subtitle={t("followUp")}
        description={t("checkBackUpdateDescription")}
        backgroundColor={waterbearLightSmokePurple}
        image={dwc4}
        direction="row"
      />
      <MoreSolutions>
        <SolutionCallout
          name={t("cdrLink")}
          description={t("cdrLinkDescription")}
          image={linkLogoSmall}
          href="/solutions/link"
          color={cdrLinkOrange}
          height={300}
        />
        <SolutionCallout
          name={t("leafcutter")}
          description={t("leafcutterDescription")}
          image={leafcutterLogo}
          href="/solutions/leafcutter"
          color={leafcutterElectricBlue}
          height={300}
        />
      </MoreSolutions>
    </>
  );
}
