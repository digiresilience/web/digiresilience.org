import { Metadata } from "next";
import { Container, Box } from "@mui/material";
import { Masonry } from "@mui/lab";
import { Client } from "@notionhq/client";
import { PostCard } from "./_components/PostCard";
import { getNotionProperty, retryableRequest } from "app/_lib/notion";
import { colors } from "app/_styles/theme";

export const metadata: Metadata = {
  title: "CDR: Blog",
};

const getPosts = async (): Promise<any> => {
  const notion = new Client({ auth: process.env.NOTION_API_KEY });
  const blogDatabaseID = process.env.NOTION_BLOG_DATABASE_ID;
  const response = await retryableRequest(() => notion.databases.query({
    database_id: blogDatabaseID,
    filter: {
      property: "Published",
      checkbox: {
        equals: true,
      },
    },
    sorts: [
      {
        property: "Published At",
        direction: "descending",
      },
    ],
  }));

  const posts = await Promise.all(
    response.results.map(async (result: any) => {
      const properties: any = {};

      for await (const key of Object.keys(result.properties)) {
        const val = result.properties[key];
        const prop = await getNotionProperty(notion, result.id, val.id);
        properties[key] = prop;
      }

      return {
        id: result.id,
        title: properties.Title[0].title.plain_text,
        slug: properties.Slug[0].rich_text.plain_text,
        author: "", // Author.relation,
        intro: properties.Intro[0].rich_text.plain_text,
        image: "", // properties.image[0]?.file.url ?? null,
        tags: properties.Tags[0].multi_select?.map((tag) => tag.name) ?? [],
        publishedAt: new Date(
          properties["Published At"][0].date.start,
        ).toLocaleDateString(),
      };
    }),
  );

  return posts;
};

export default async function Page() {
  const posts = await getPosts();
  const { beige } = colors;

  return (
    <Box
      sx={{
        backgroundColor: beige,
        padding: {
          xs: 1,
          sm: 4
        },
        pb: 12,
      }}
    >
      <Container>
        <Masonry columns={2} spacing={4}>
          {posts.map((post) => (
            <PostCard key={post.id} post={post} />
          ))}
        </Masonry>
      </Container>
    </Box>
  );
}
