import { Metadata } from "next";
import { Container } from "@mui/material";
import { Client } from "@notionhq/client";
import { Post as PostBody } from "../_components/Post";
import { getNotionProperty, retryableRequest } from "app/_lib/notion";

export const metadata: Metadata = {
  title: "CDR: Blog",
};

const getPost = async (postID: string): Promise<any> => {
  const notion = new Client({ auth: process.env.NOTION_API_KEY });
  const databaseID = process.env.NOTION_BLOG_DATABASE_ID;
  const response = await retryableRequest(() => notion.databases.query({
    database_id: databaseID,
    filter: {
      and: [
        {
          property: "Published",
          checkbox: {
            equals: true,
          },
        },
        {
          property: "Slug",
          rich_text: {
            equals: postID,
          },
        },
      ],
    },
    sorts: [
      {
        property: "Published At",
        direction: "descending",
      },
    ],
  }));

  const posts = await Promise.all(
    response.results.map(async (result: any) => {
      const blockRequest = await retryableRequest(() => notion.blocks.children.list({
        block_id: result.id,
        page_size: 50,
      }));

      const properties: any = {};

      for await (const key of Object.keys(result.properties)) {
        const val = result.properties[key];
        const prop = await getNotionProperty(notion, result.id, val.id);
        properties[key] = prop;
      }

      return {
        id: result.id,
        title: properties.Title[0].title.plain_text,
        author: "", // Author.relation,
        image: "", // properties.image[0]?.file.url ?? null,
        tags: properties.Tags[0].multi_select?.map((tag) => tag.name) ?? [],
        publishedAt: new Date(
          properties["Published At"][0].date.start,
        ).toLocaleDateString(),
        text: blockRequest.results,
      };
    }),
  );

  return posts[0];
};

type PageProps = {
  params: {
    postID: string;
  };
};

export default async function Page({ params: { postID } }: PageProps) {
  const post = await getPost(postID);

  return (
    <Container maxWidth="md" sx={{ mb: 5 }}>
      <PostBody post={post} />
    </Container>
  );
}

export async function generateStaticParams() {
  const notion = new Client({ auth: process.env.NOTION_API_KEY });
  const databaseID = process.env.NOTION_BLOG_DATABASE_ID;

  const response = await retryableRequest(() => notion.databases.query({
    database_id: databaseID,
    filter: {
      property: "Published",
      checkbox: {
        equals: true,
      },
    },
  }));

  const params = await Promise.all(
    response.results.map(async (result: any) => {
      const properties: any = {};
      for await (const key of Object.keys(result.properties)) {
        const val = result.properties[key];
        const prop = await getNotionProperty(notion, result.id, val.id);
        properties[key] = prop;
      }

      return {
        // @ts-ignore
        postID: properties.Slug[0].rich_text.plain_text,
      };
    }),
  );
  
  return params;
}
