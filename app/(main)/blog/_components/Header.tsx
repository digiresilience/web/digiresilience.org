"use client";

import { FC } from "react";
import { Box, Grid, Stack, TextField, Chip } from "@mui/material";
import { colors, typography } from "app/_styles/theme";

export const Header: FC = () => {
  const { white, hazyMint } = colors;
  const { bodySmall } = typography;

  return (
    <Grid item>
      <TextField
        sx={{ width: "100%", backgroundColor: white, mb: 2 }}
        size="small"
        placeholder="Search posts..."
      />
      <Stack direction="row" spacing={1} justifyContent="space-between">
        <Stack direction="row" spacing={1}>
          <Chip
            label="Documentation"
            size="small"
            sx={{ borderRadius: "5px", backgroundColor: hazyMint }}
          />
          <Chip
            label="DSX"
            size="small"
            sx={{ borderRadius: "5px", backgroundColor: hazyMint }}
          />
        </Stack>
        <Box component="p" sx={{ ...bodySmall }}>
          View Archives
        </Box>
      </Stack>
    </Grid>
  );
};
