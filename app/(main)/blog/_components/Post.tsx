"use client";

import { FC } from "react";
import { Card, Grid, Box, Stack, Chip } from "@mui/material";
import { Render as RenderNotion } from "@9gustin/react-notion-render";
import { colors, typography } from "app/_styles/theme";

interface PostProps {
  post: {
    id: string;
    title: string;
    author: string;
    publishedAt: string;
    text: any;
    tags: string[];
  };
}

export const Post: FC<PostProps> = ({
  post: { id, title, author, publishedAt, text, tags },
}) => {
  const { white, hazyMint } = colors;
  const { h3, body } = typography;

  return (
    <Card key={id} sx={{ backgroundColor: white, padding: 3 }}>
      <Grid item container direction="column">
        <Grid
          item
          container
          direction="row"
          justifyContent="space-between"
          wrap="nowrap"
        >
          <Grid item container direction="row">
            <Grid item>{}</Grid>
            <Grid item>{author}</Grid>
          </Grid>
          <Grid item>{publishedAt}</Grid>
        </Grid>
        <Grid item>
          <Box component="h3" sx={{ ...h3 }}>
            {title}
          </Box>
          <Grid item>
            <Stack direction="row" spacing={1}>
              {tags.map((tag) => (
                <Chip
                  key={tag}
                  label={tag}
                  size="small"
                  sx={{ borderRadius: "5px", backgroundColor: hazyMint }}
                />
              ))}
            </Stack>
          </Grid>
        </Grid>
        <Grid item>
          <Box component="body" sx={{ ...body }}>
            <RenderNotion blocks={text} classNames />
          </Box>
        </Grid>
      </Grid>
    </Card>
  );
};
