"use client";

import { FC } from "react";
import Link from "next/link";
import { Card, Grid, Box, Stack, Chip } from "@mui/material";
import { ArrowRight as ArrowRightIcon } from "@mui/icons-material";
import { colors, typography } from "app/_styles/theme";

interface PostCardProps {
  post: {
    id: string;
    slug: string;
    title: string;
    author: string;
    intro: string;
    image?: string;
    publishedAt: string;
    tags: string[];
  };
}

export const PostCard: FC<PostCardProps> = ({
  post: { id, slug, title, author, intro, image, publishedAt, tags },
}) => {
  const { white, hazyMint, cdrLinkOrange, lightBlack } = colors;
  const { h3, body, bodySmall } = typography;

  return (
    <Box>
      <Link href={`/blog/${slug}`}>
        <Card
          key={id}
          sx={{
            backgroundColor: white,
            padding: 3,
            cursor: "pointer",
          }}
        >
          <Grid item container direction="column" spacing={1}>
            <Grid
              item
              container
              direction="row"
              justifyContent="space-between"
              wrap="nowrap"
            >
              <Grid
                item
                container
                direction="row"
                justifyContent="space-between"
                xs={6}
              >
                <Grid item>{}</Grid>
                <Grid item>
                  <Box component="p" sx={{ ...bodySmall, color: lightBlack }}>
                    {author ?? "Anonymous"}
                  </Box>
                </Grid>
              </Grid>
              <Grid item xs={6}>
                <Box
                  component="p"
                  sx={{ ...bodySmall, color: lightBlack, textAlign: "right" }}
                >
                  {new Date(publishedAt).toLocaleDateString(undefined, {
                    dateStyle: "medium",
                  })}
                </Box>
              </Grid>
            </Grid>
            <Grid item>
              <Box
                component="h3"
                sx={{ ...h3, m: 0, mt: 2, p: 0, mb: -0.5, textAlign: "left" }}
              >
                {title}
              </Box>
            </Grid>
            <Grid item>
              <Stack direction="row" spacing={1}>
                {tags.map((tag) => (
                  <Chip
                    key={tag}
                    label={tag}
                    size="small"
                    sx={{ borderRadius: "5px", backgroundColor: hazyMint }}
                  />
                ))}
              </Stack>
            </Grid>
            {image && (
              <Grid item sx={{ maxWidth: 500 }}>
                <img src={image} alt="" style={{ width: "100%" }} />
              </Grid>
            )}
            <Grid item>
              <Box component="p" sx={{ ...body, m: 0, p: 0 }}>
                {intro}
              </Box>
            </Grid>
            <Grid item>
              <Box
                component="p"
                sx={{
                  color: cdrLinkOrange,
                  fontWeight: 700,
                  textAlign: "right",
                  mt: -1,
                }}
              >
                More{" "}
                <ArrowRightIcon
                  sx={{ fontSize: 32, mb: "-11px", ml: "-8px" }}
                />
              </Box>
            </Grid>
          </Grid>
        </Card>
      </Link>
    </Box>
  );
};
