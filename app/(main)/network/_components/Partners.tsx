"use client";

import { FC } from "react";
import { Grid, useTheme, useMediaQuery} from "@mui/material";
import defenders from "app/_images/defenders.jpg";
import dsa from "app/_images/dsa.png";
import greenhost from "app/_images/greenhost.png";
import guardian from "app/_images/guardian.png";
import insm from "app/_images/insm.png";
import miann from "app/_images/miann.png";
import secdev from "app/_images/secdev.png";
import tor from "app/_images/tor.png";
import { Button } from "app/_components/common/Button";
import { Partner } from "./Partner";
import { colors } from "app/_styles/theme";

export const Partners: FC = () => {
  const { cdrLinkOrange } = colors;

  const theme = useTheme();
  const mobile = useMediaQuery(theme.breakpoints.down('sm'));

  return (
    <Grid container direction="column" alignItems="center">
      <Grid
        item
        container
        direction="row"
        spacing={4}
        justifyContent="space-around"
        alignItems="center"
        alignContent="center"
        sx={{ overflow: mobile ? "hidden" : "inherit" }}
      >
        <Partner name="Defenders" description="" image={defenders} />
        <Partner name="DSA" description="" image={dsa} />
        <Partner name="Greenhost" description="" image={greenhost} />
        <Partner name="Guardian" description="" image={guardian} />
        <Partner name="INSM" description="" image={insm} />
        <Partner name="MIANN" description="" image={miann} />
        <Partner name="SecDev" description="" image={secdev} />
        <Partner name="Tor" description="" image={tor} />
      </Grid>
      <Grid item>
        <Button
          text="Join the Network"
          color={cdrLinkOrange}
          href="/partners"
        />
      </Grid>
    </Grid>
  );
};
