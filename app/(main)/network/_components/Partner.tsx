"use client";

import { FC } from "react";
import Image, { StaticImageData } from "next/image";
import { Grid } from "@mui/material";
import { typography } from "app/_styles/theme";

interface PartnerProps {
  name: string;
  description: string;
  image: StaticImageData;
}

export const Partner: FC<PartnerProps> = ({ name, description, image }) => {
  const { body } = typography;

  return (
    <Grid
      item
      container
      direction="column"
      alignItems="center"
      xs={12}
      sm={6}
      md={4}
      spacing={1}
    >
      <Grid
        item
        sx={{
          textAlign: "center",
        }}
      >
        <Image src={image} alt={name} width={200} height={120} />
      </Grid>
      <Grid item component="p" sx={{ ...body, textAlign: "center", mt: 2 }}>
        {description}
      </Grid>
    </Grid>
  );
};
