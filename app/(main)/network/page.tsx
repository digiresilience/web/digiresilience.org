import { Metadata } from "next";
import { Partners } from "./_components/Partners";
import { TextHeader } from "app/_components/common/TextHeader";

export const metadata: Metadata = {
  title: "CDR: Network",
};

export default async function Page() {
  return (
    <>
      <TextHeader text="Network Partners" />
      <Partners />
    </>
  );
}
