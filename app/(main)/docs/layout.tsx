import {  ReactNode } from "react";
import { Box } from "@mui/material";
import { colors } from "app/_styles/theme";
import DocumentSidebar from "./_components/DocumentSidebar";
import { getAllDocsPages } from "app/_lib/notion";

export default async function DocsLayout({ children }: { children: ReactNode }) {
  const { cdrLinkOrange } = colors;

  const allDocs = await getAllDocsPages();
  return (
    <>
      <Box sx={{ display: "flex", height: "100%", borderTop: `1px solid ${cdrLinkOrange}`, minHeight: "50vh" }}>
        <DocumentSidebar docs={allDocs} />
        {children as any}
      </Box>
    </>
  );
}
