import { Metadata } from "next";
import { redirect } from "next/navigation";

export const metadata: Metadata = {
  title: "CDR: Docs",
};

export default async function Page() {
  redirect("/docs/welcome");
}
