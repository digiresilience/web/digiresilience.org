import { Metadata } from "next";
import { Container } from "@mui/material";
import { Client } from "@notionhq/client";
import { PageBody } from "../_components/PageBody";
import { getDocPage, getNotionProperty, retryableRequest } from "app/_lib/notion";

export const metadata: Metadata = {
  title: "CDR: Docs",
};

type PageProps = {
  params: {
    doc: string[];
  };
};

export default async function Page({ params: { doc } }: PageProps) {
  const page = await getDocPage(doc);

  return (
    <Container maxWidth="md" sx={{ mb: 5 }}>
      <PageBody page={page} />
    </Container>
  );
}

export async function generateStaticParams() {
  const notion = new Client({ auth: process.env.NOTION_API_KEY });
  const databaseID = process.env.NOTION_DOCS_DATABASE_ID;

  const response = await retryableRequest(() => notion.databases.query({
    database_id: databaseID,
    filter: {
      property: "Published",
      checkbox: {
        equals: true,
      },
    },
  }));

  const params = await Promise.all(
    response.results.map(async (result: any) => {
      const val = result.properties.Path;
      const path = await getNotionProperty(notion, result.id, val.id);
      
      return {
        doc: path[0].rich_text.plain_text.split("/"),
      };
    }),
  );

  return params;
}
