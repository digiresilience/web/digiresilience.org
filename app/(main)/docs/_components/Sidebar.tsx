"use client";

import { FC } from "react";
import {
  Box,
  Grid,
  Typography,
  List,
  ListItemButton,
  ListItemIcon,
  ListItemText,
  Drawer,
} from "@mui/material";
import {
  ExpandCircleDown as ExpandCircleDownIcon,
  AccountCircle as AccountCircleIcon,
  Chat as ChatIcon,
  PermPhoneMsg as PhoneIcon,
  WhatsApp as WhatsAppIcon,
  Facebook as FacebookIcon,
} from "@mui/icons-material";
import { usePathname } from "next/navigation";
import Link from "next/link";
import { fonts, colors } from "app/_styles/theme";

const openWidth = 270;
const closedWidth = 100;

export const SidebarItem = ({
  name,
  href,
  Icon,
  iconSize,
  inset = false,
  open = true,
  target = "_self",
}: any) => {
  const { cdrLinkOrange, helpYellow, almostBlack } = colors;
  const pathname = usePathname();

  return (
    <Link href={href} target={target}>
      <ListItemButton
        sx={{
          p: 0,
          mb: 1,
          bl: iconSize === 0 ? "1px solid white" : "inherit",
        }}
        selected={pathname.endsWith(href)}
      >
        {iconSize > 0 ? (
          <ListItemIcon
            sx={{
              color: `white`,
              minWidth: 0,
              mr: 2,
              textAlign: "center",
              margin: open ? "0 8 0 0" : "0 auto",
            }}
          >
            <Box
              sx={{
                width: iconSize,
                height: iconSize,
                mr: 0.5,
                mt: "-4px",
              }}
            >
              <Icon />
            </Box>
          </ListItemIcon>
        ) : (
          <Box
            sx={{
              width: 30,
              height: "28px",
              position: "relative",
              ml: "9px",
              mr: "1px",
            }}
          >
            <Box
              sx={{
                width: "1px",
                height: "56px",
                backgroundColor: cdrLinkOrange,
                position: "absolute",
                left: "3px",
                top: "-10px",
              }}
            />
            <Box
              sx={{
                width: "42px",
                height: "42px",
                position: "absolute",
                top: "-27px",
                left: "3px",
                border: `solid 1px ${cdrLinkOrange}`,
                borderColor: `transparent transparent transparent ${cdrLinkOrange}`,
                borderRadius: "60px",
                rotate: "-35deg",
              }}
            />
          </Box>
        )}
        {open && (
          <ListItemText
            inset={inset}
            primary={
              <Typography
                variant="body1"
                sx={{
                  fontSize: 16,
                  fontWeight: "bold",
                  border: 0,
                  textAlign: "left",
                  color: "white",
                }}
              >
                {name}
              </Typography>
            }
          />
        )}
      </ListItemButton>
    </Link>
  );
};

interface SidebarProps {
  open: boolean;
  setOpen: (open: boolean) => void;
  children: any;
}

export const Sidebar: FC<SidebarProps> = ({ open, setOpen, children }) => {
  const { poppins } = fonts;
  const { cdrLinkOrange, beige, helpYellow, almostBlack } = colors;

  return (
    <Drawer
      sx={{ width: open ? openWidth : closedWidth, flexShrink: 0 }}
      variant="permanent"
      anchor="left"
      open={open}
      PaperProps={{
        sx: {
          top: 100,
          zIndex: -1,
          width: open ? openWidth : closedWidth,
          border: 0,
          overflow: "visible",
        },
      }}
    >
      <Grid
        container
        direction="column"
        justifyContent="space-between"
        wrap="nowrap"
        spacing={0}
        sx={{ background: beige, borderRight: `1px solid ${cdrLinkOrange}`,  height: "100%", p: 2 }}
      >
        <Grid
          item
          container
          direction="column"
          sx={{
            mt: "6px",
            overflow: "scroll",
            scrollbarWidth: "none",
            msOverflowStyle: "none",
            "&::-webkit-scrollbar": { display: "none" },
          }}
          flexGrow={1}
        >
          <List
            component="nav"
            sx={{
              a: {
                textDecoration: "none",

                ".MuiListItemButton-root": {
                  p: 1,
                  borderRadius: 2,
                  "&:hover": {
                    background: "#555",
                  },
                  ".MuiTypography-root": {
                    p: {
                      color: "#999 !important",
                      fontSize: 16,
                    },
                  },
                },
                ".Mui-selected": {
		  background: beige,
                  ".MuiTypography-root": {
                    p: {
		      color: `${cdrLinkOrange} !important`,
                      fontSize: 16,
                    },
                  },
                },
              },
            }}
          >
            {children}
          </List>
        </Grid>
      </Grid>
    </Drawer>
  );
};
