"use client";

import * as React from "react";
import { colors } from "app/_styles/theme";
import { SimpleTreeView } from "@mui/x-tree-view/SimpleTreeView";
import { useRouter } from "next/navigation";
import { useTreeItem2Utils } from "@mui/x-tree-view/hooks/useTreeItem2Utils";
import { UseTreeItem2ContentSlotOwnProps } from "@mui/x-tree-view/useTreeItem2";
import { TreeItem2, TreeItem2Props } from "@mui/x-tree-view/TreeItem2";
import { styled, alpha } from "@mui/material";
import { usePathname } from "next/navigation";

interface DocumentSidebarProps {
  docs: { id: string; section: string; title: String; path: string; position: number }[];
}

export default function DocumentSidebar(props: DocumentSidebarProps) {
  const allDocs = props.docs;
  const idToLinkMap: { [key: string]: string } = {};
  const expandedItems: string[] = [];
  const selectedItems: string[] = [];

  const { beige, cdrLinkOrange } = colors;

  const router = useRouter();
  const pathname = usePathname();

  const StyledTreeItemRoot = styled(TreeItem2)(({ theme }) => ({
    [`& .Mui-focused, & .Mui-selected, & .Mui-selected.Mui-focused`]: {
      backgroundColor: alpha(cdrLinkOrange, 0.1),
      color: "black",
    },
  })) as unknown as typeof TreeItem2;

  interface CustomTreeItemProps {
    level: number;
  }

  const CustomTreeItem = React.forwardRef(function MyTreeItem(
    props: TreeItem2Props & CustomTreeItemProps,
    ref: React.Ref<HTMLLIElement>
  ) {
    const { interactions } = useTreeItem2Utils({
      itemId: props.itemId,
      children: props.children,
    });

    const handleContentClick: UseTreeItem2ContentSlotOwnProps["onClick"] = (event) => {
      const path = idToLinkMap[props.itemId];
      if (path) {
        event.defaultMuiPrevented = true;
        interactions.handleSelection(event);
      }
    };

    const handleIconContainerClick = (event: React.MouseEvent) => {
      interactions.handleExpansion(event);
    };

    function HiddenExpander() {
      return <></>;
    }

    return (
      <StyledTreeItemRoot
        {...props}
        ref={ref}
        slots={
          // Hide top level expander
          props.level == 0
            ? {
                iconContainer: HiddenExpander,
              }
            : {}
        }
        slotProps={{
          content: { onClick: handleContentClick },
          iconContainer: { onClick: handleIconContainerClick },
        }}
      />
    );
  });

  interface Node {
    id: string;
    section: string;
    title: string;
    path: string;
    position: number;
    children?: Node[];
    parent?: Node;
  }

  function createTree(data: any[]): Node {
    const root: Node = { id: "root", title: "root", section: "root", position: 0, path: "" };
    const map: { [key: string]: Node } = { root: root };

    data.forEach((obj) => {
      const pathParts = obj.path.split("/");
      pathParts[0] = obj.section;
      let parent = root;

      pathParts.forEach((part) => {
        const key = parent.title + "/" + part;

        if (!map[key]) {
          const newNode: Node = {
            id: "",
            path: obj.path,
            title: obj.title,
            section: obj.section,
            position: obj.position,
            parent: parent,
          };
          map[key] = newNode;

          if (!parent.children) {
            parent.children = [];
          }

          parent.children.push(newNode);
        }

        parent = map[key];

        if (part == pathParts[pathParts.length - 1]) {
          // Leaf
          Object.assign(map[key], {
            id: obj.id,
            path: obj.path,
            title: obj.title,
            section: obj.section,
            position: obj.position,
          });
        }
      });
    });

    return root;
  }

  const menuTree = createTree(allDocs);
  const mapLevel = (objects, level) => {
    function compareFn(a, b) {
      if (a.path === "welcome" || a.position < b.position) {
        return -1;
      } else if (b.path === "welcome" || a.position > b.position) {
        return 1;
      }
      return 0;
    }

    objects.sort(compareFn);

    return objects.map((obj) => {
      const title = level == 0 ? obj.section : obj.title;
      const id = level == 0 ? obj.section : obj.path;
      obj.itemId = id; // For use in parent expansion below
      if ((level > 0 && obj.path !== "") || !obj["children"]) {
        // Leaf, add to link map
        idToLinkMap[id] = obj.path;
      }
      if (level == 0) {
        expandedItems.push(id);
      }
      if (obj.id !== "" && obj.path !== "" && (pathname.endsWith(obj.path) || pathname.endsWith(obj.path + "/"))) {
        // The current path
        selectedItems.push(id);

        // Make sure it is shown, by ensuring all parents are expanded
        let parent = obj.parent;
        while (parent) {
          if (!expandedItems.includes[parent.itemId]) {
            expandedItems.push(parent.itemId);
          }
          parent = parent.parent;
        }
      }
      return (
        <CustomTreeItem label={title} itemId={id} key={id} level={level}>
          {obj["children"] ? mapLevel(obj["children"], level + 1) : ""}
        </CustomTreeItem>
      );
    });
  };

  let userElements = mapLevel(menuTree.children, 0);

  const handleSelectedItemsChange = (event, itemId) => {
    const path = idToLinkMap[itemId];
    if (path) {
      router.push("/docs/" + path);
    }
  };

  return (
    <SimpleTreeView
      defaultExpandedItems={expandedItems}
      defaultSelectedItems={selectedItems}
      sx={{ background: beige, borderRight: `1px solid ${cdrLinkOrange}`, p: 2, maxWidth: "30%", width: "300px" }}
      onSelectedItemsChange={handleSelectedItemsChange}
    >
      <style jsx global>{`
        body > .MuiGrid-root {
          display: block !important;
        }
      `}</style>
      {userElements}
    </SimpleTreeView>
  );
}
