"use client";

import { FC } from "react";
import { Grid, Box } from "@mui/material";
import { Render as RenderNotion } from "@9gustin/react-notion-render";
import { colors, typography } from "app/_styles/theme";

interface PageBodyProps {
  page: {
    id: string;
    title: string;
    text: any;
  };
}

export const PageBody: FC<PageBodyProps> = ({ page: { title, text } }) => {
  const { white } = colors;
  const { h1, h2, h3, body } = typography;

  return (
    <Box sx={{ backgroundColor: white, padding: 3 }}>
      <Grid item container direction="column">
        <Grid item>
          <Box component="h3" sx={h3}>
            {title}
          </Box>
        </Grid>
        <Grid item sx={{
              ".rnr-heading_1": h1,
              ".rnr-heading_2": h2,
              ".rnr-heading_3": h3,
              ".rnr-paragraph": body
            }}>
            <RenderNotion blocks={text} classNames />
        </Grid>
      </Grid>
    </Box>
  );
};
