"use client";

import { FC } from "react";
import { Grid, Box, Button } from "@mui/material";
import Link from "next/link";
import { colors, typography } from "app/_styles/theme";

interface FooterLinksProps {
  title: string;
  links: Array<{ name: string; href: string }>;
}

export const FooterLinks: FC<FooterLinksProps> = ({ title, links }) => {
  const { white } = colors;
  const { h5 } = typography;

  return (
    <Grid
      item
      container
      direction="column"
      spacing={1}
      alignItems="flex-start"
      xs={6}
      sm={4}
    >
      <Grid item>
        <Box
          component="h5"
          sx={{ ...h5, textAlign: "left", color: white, padding: 0, margin: 0 }}
        >
          {title}
        </Box>
      </Grid>
      {links.map((link) => (
        <Grid
          item
          key={link.name}
          alignItems="flex-start"
          alignContent="flex-start"
          justifyContent="flex-start"
          sx={{
            textAlign: "left",
            padding: 0,
            margin: 0,
          }}
        >
          <Link href={link.href}>
            <Button
              variant="text"
              sx={{
                color: white,
                margin: 0,
                padding: 0,
                textAlign: "left",
                minWidth: 0,
                ":hover": {
                  fontWeight: 700,
                },
              }}
            >
              {link.name}
            </Button>
          </Link>
        </Grid>
      ))}
    </Grid>
  );
};
