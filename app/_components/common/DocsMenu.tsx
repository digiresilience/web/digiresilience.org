"use client";

import { FC } from "react";
import Link from "next/link";
import { Box, Grid, Button, Menu, MenuItem } from "@mui/material";
import { ArrowDropDown as ArrowDropDownIcon } from "@mui/icons-material";
import {
  usePopupState,
  bindTrigger,
  bindMenu,
} from "material-ui-popup-state/hooks";
import { colors, typography } from "app/_styles/theme";

type DocsMenuProps = {
  docs?: any;
};

export const DocsMenu: FC<DocsMenuProps> = ({ docs = [] }) => {
  const { almostBlack, cdrLinkOrange } = colors;
  const { h4, h5 } = typography;

  const docsPopupState = usePopupState({
    variant: "popover",
    popupId: "docs",
  });
  const buttonStyles: any = {
    ...h5,
    color: almostBlack,
    whiteSpace: "nowrap",
    textAlign: "left",
    margin: 0,
  };
  const buttonMenuItemStyles: any = {
    ...h5,
    color: almostBlack,
    textAlign: "left",
    lineHeight: "100%",
    margin: 0,
  };

  const sections = docs.reduce((prev, cur) => {
    const key = cur.section;
    const out = { ...prev };
    if (prev[key]) {
      out[key].push(cur);
    } else {
      out[key] = [cur];
    }
    return out;
  }, {});

  return (
    <Grid item>
      <Button variant="text" sx={buttonStyles} {...bindTrigger(docsPopupState)}>
        Docs <ArrowDropDownIcon />
      </Button>
      <Menu {...bindMenu(docsPopupState)} elevation={2}>
        {Object.keys(sections).map((key) => {
          const subitems = sections[key].sort(
            (a, b) => b.position - a.position,
          );

          return (
            <Box key={key}>
              <Box
                component="h4"
                sx={{ ...h4, color: cdrLinkOrange, pl: 3, pt: 3, m: 0 }}
              >
                {key}
              </Box>

              {subitems.map((subitem) => (
                <MenuItem
                  key={subitem.path}
                  onClick={docsPopupState.close}
                  sx={{ textAlign: "left" }}
                >
                  <Link href={`/docs/${subitem.path}`}>
                    <Button variant="text" sx={buttonMenuItemStyles}>
                      {subitem.title}
                    </Button>
                  </Link>
                </MenuItem>
              ))}
            </Box>
          );
        })}
      </Menu>
    </Grid>
  );
};
