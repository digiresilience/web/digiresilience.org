"use client";

import { FC } from "react";
import Image, { StaticImageData } from "next/image";
import { Grid, Box } from "@mui/material";
import { typography } from "app/_styles/theme";

interface HorizontalCalloutProps {
  title: string;
  description: string;
  image: StaticImageData;
}

export const HorizontalCallout: FC<HorizontalCalloutProps> = ({
  title,
  description,
  image,
}) => {
  const { h4, body } = typography;

  return (
    <Grid
      container
      item
      direction="row"
      xs={12}
      sm={6}
      wrap="nowrap"
      rowSpacing={6}
    >
      <Grid item sx={{ marginRight: 2 }}>
        <Image src={image} alt="" width={80} height={80} />
      </Grid>
      <Grid item container direction="column">
        <Grid item>
          <Box component="h4" sx={{ ...h4, margin: 0 }}>
            {title}
          </Box>
        </Grid>
        <Grid item>
          <Box component="p" sx={{ ...body }}>
            {description}
          </Box>
        </Grid>
      </Grid>
    </Grid>
  );
};
