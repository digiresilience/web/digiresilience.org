"use client";

import { FC, useState } from "react";
import { Grid, Box, TextField, Button, Alert, Snackbar } from "@mui/material";
import { t } from "app/_lib/helpers";
import { colors, typography } from "app/_styles/theme";

export const Subscribe: FC = () => {
  const { beige, white, cdrLinkOrange } = colors;
  const { h3, body } = typography;
  const [firstName, setFirstName] = useState("");
  const [lastName, setLastName] = useState("");
  const [emailAddress, setEmailAddress] = useState("");
  const [language, setLanguage] = useState("1");
  const [confirmationVisible, setConfirmationVisible] = useState(false);
  const handleSubscribe = async (e: any) => {
    e.preventDefault();
    const formData = new FormData();
    formData.append("FNAME", firstName);
    formData.append("LNAME", lastName);
    formData.append("EMAIL", emailAddress);
    if (language === "2") {
      formData.append("group[119][2]", "2");
    } else {
      formData.append("group[119][2]", "2");
    }
    await fetch(
      "https://digiresilience.us19.list-manage.com/subscribe/post?u=14cb589b4762cb7b2e9f3d5ec&amp;id=f0b3eb0d5f",
      { method: "POST", body: formData, mode: "no-cors" },
    );
    setFirstName("");
    setLastName("");
    setEmailAddress("");
    setLanguage("1");
    setConfirmationVisible(true);
  };

  return (
    <>
      <Snackbar
        open={confirmationVisible}
        autoHideDuration={6000}
        onClose={() => setConfirmationVisible(false)}
        anchorOrigin={{ vertical: "bottom", horizontal: "right" }}
      >
        <Alert severity="success">Success. Thanks for subscribing!</Alert>
      </Snackbar>
      <Grid
        container
        direction="column"
        spacing={2}
        sx={{ backgroundColor: beige, padding: 3 }}
      >
        <Grid item>
          <Box component="h3" sx={{ ...h3 }}>
            {t("stayInTheLoop")}
          </Box>
        </Grid>
        <Grid item>
          <Box component="p" sx={{ ...body, mt: 0 }}>
            {t("stayInTheLoopDescription")}
          </Box>
        </Grid>
        <Grid item container direction="row" spacing={3}>
          <Grid item xs={12} sm={6}>
            <TextField
              sx={{ backgroundColor: white }}
              variant="outlined"
              size="small"
              fullWidth
              placeholder={t("firstName")}
              value={firstName}
              onChange={(e) => setFirstName(e.target.value)}
            />
          </Grid>
          <Grid item xs={12} sm={6}>
            <TextField
              sx={{ backgroundColor: white }}
              variant="outlined"
              size="small"
              fullWidth
              placeholder={t("lastName")}
              value={lastName}
              onChange={(e) => setLastName(e.target.value)}
            />
          </Grid>
        </Grid>
        <Grid item xs={12}>
          <TextField
            sx={{ backgroundColor: white, width: "100%" }}
            variant="outlined"
            size="small"
            placeholder={t("emailAddress")}
            value={emailAddress}
            onChange={(e) => setEmailAddress(e.target.value)}
          />
        </Grid>
        <Grid item container direction="row-reverse">
          <Grid item>
            <Button
              variant="contained"
              disableElevation
              sx={{
                fontFamily: "Poppins, sans-serif",
                fontWeight: 700,
                color: white,
                borderRadius: 999,
                backgroundColor: cdrLinkOrange,
                padding: "6px 30px",
                margin: "20px 0px",
                whiteSpace: "nowrap",
              }}
              onClick={handleSubscribe}
            >
              {t("subscribe")}
            </Button>
          </Grid>
        </Grid>
      </Grid>
    </>
  );
};
