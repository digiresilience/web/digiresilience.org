"use client";

import { FC } from "react";
import Image, { StaticImageData } from "next/image";
import { Grid, Box } from "@mui/material";
import { typography } from "app/_styles/theme";

interface VerticalCalloutProps {
  title: string;
  description: string;
  image: StaticImageData;
}

export const VerticalCallout: FC<VerticalCalloutProps> = ({
  title,
  description,
  image,
}) => {
  const { h3, body } = typography;

  return (
    <Grid
      container
      item
      direction="column"
      xs={12}
      sm={4}
      alignContent="center"
      alignItems="center"
    >
      <Grid item>
        <Image src={image} alt={title} height={95} width={95} />
      </Grid>
      <Grid item>
        <Box component="h3" sx={{ ...h3, mb: 0, mt: 2 }}>
          {title}
        </Box>
      </Grid>
      <Grid item>
        <Box component="p" sx={{ ...body, textAlign: "center", margin: 0 }}>
          {description}
        </Box>
      </Grid>
    </Grid>
  );
};
