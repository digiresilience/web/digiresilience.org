"use client";

import { FC } from "react";
import { Container, Grid, Box, Hidden } from "@mui/material";
import defenders from "app/_images/defenders.jpg";
import dsa from "app/_images/dsa.png";
import greenhost from "app/_images/greenhost.png";
import guardian from "app/_images/guardian.png";
import insm from "app/_images/insm.png";
import miann from "app/_images/miann.png";
import secdev from "app/_images/secdev.png";
import tor from "app/_images/tor.png";
import { NetworkPartner } from "./NetworkPartner";
import { typography } from "app/_styles/theme";
import { t } from "app/_lib/helpers";

export const Network: FC = () => {
  const { h2, body } = typography;

  return (
    <Box sx={{ padding: 3 }}>
      <Container sx={{ padding: 3 }}>
        <Grid container direction="column" alignItems="center">
          <Grid item>
            <Box component="h2" sx={{ ...h2 }}>
              {t("digitalSecurityNetwork")}
            </Box>
          </Grid>
          <Grid item>
            <Box component="p" sx={{ ...body, paddingBottom: 3 }}>
              {t("digitalSecurityNetworkDescription")}
            </Box>
          </Grid>
          <Grid
            item
            container
            direction="row"
            justifyContent="space-around"
            spacing={2}
          >
            <Hidden smDown>
              <NetworkPartner name="Tor" image={tor} />
              <NetworkPartner name="Defend Defenders" image={defenders} />
              <NetworkPartner name="Digital Society Africa" image={dsa} />
              <NetworkPartner
                name="Iraqi Network for Socail Media"
                image={insm}
              />
            </Hidden>
            <NetworkPartner name="Greenhost" image={greenhost} />
            <NetworkPartner name="Guardian Project" image={guardian} />
            <Hidden smDown>
              <NetworkPartner name="SecDev Foundation" image={secdev} />
              <NetworkPartner name="MIANN" image={miann} />
            </Hidden>
          </Grid>
        </Grid>
      </Container>
    </Box>
  );
};
