"use client";

/* eslint-disable jsx-a11y/anchor-is-valid */
/* eslint-disable react/jsx-props-no-spreading */
import { FC, useState } from "react";
import Image from "next/image";
import {
  AppBar,
  Grid,
  Box,
  Button,
  Container,
  Menu,
  MenuItem,
  SwipeableDrawer,
  IconButton,
  useTheme,
  useMediaQuery,
} from "@mui/material";
import {
  Menu as MenuIcon,
  ArrowDropDown as ArrowDropDownIcon,
} from "@mui/icons-material";
import Link from "next/link";
import {
  usePopupState,
  bindTrigger,
  bindMenu,
} from "material-ui-popup-state/hooks";
import logoSmall from "app/_images/logo-small.png";
import { Button as CDRButton } from "./Button";
import { colors, typography } from "app/_styles/theme";
import { t } from "app/_lib/helpers";

type TopNavProps = {
  docs?: any;
};

export const TopNav: FC<TopNavProps> = () => {
  const { cdrLinkOrange, almostBlack, white } = colors;
  const { h4, h5 } = typography;

  const theme = useTheme();
  const useCompactHeader = useMediaQuery(theme.breakpoints.down("md"));
  const useSmallerSize = useMediaQuery(theme.breakpoints.down("lg"));

  const [menuOpen, setMenuOpen] = useState(false);
  const caseStudiesPopupState = usePopupState({
    variant: "popover",
    popupId: "caseStudies",
  });
  const techSolutionsPopupState = usePopupState({
    variant: "popover",
    popupId: "techSolutions",
  });
  const aboutPopupState = usePopupState({
    variant: "popover",
    popupId: "about",
  });
  const buttonStyles: any = {
    ...h5,
    color: almostBlack,
    whiteSpace: "nowrap",
    margin: 0,
    fontSize: {
      xs: "0.8rem",
      lg: "1rem",
    },
  };
  const buttonMenuItemStyles: any = {
    ...h5,
    color: almostBlack,
    textAlign: "left",
    lineHeight: "100%",
    margin: 0,
  };

  return (
    <AppBar
      position="fixed"
      elevation={0}
      sx={{
        backgroundColor: white,
        marginBottom: 180,
        opacity: 0.98,
      }}
    >
      <Container>
        <Grid
          container
          direction="row"
          justifyContent="space-between"
          wrap="nowrap"
          sx={{ padding: 2 }}
          alignItems="center"
        >
          <Grid item sx={{ mr: 2 }}>
            <Image src={logoSmall} alt="CDR Logo" width={40} height={40} />
          </Grid>
          <Grid item sm={5}>
            <Link href="/">
              <Box
                sx={{
                  color: cdrLinkOrange,
                  fontFamily: "Roboto, sans-serif",
                  fontSize: useCompactHeader ? 13 : 18,
                  fontWeight: 700,
                  textTransform: "uppercase",
                  letterSpacing: 0.8,
                  whiteSpace: "nowrap",
                  lineHeight: "120%",
                  textAlign: useCompactHeader ? "center" : "left",
                  textDecoration: "none",
                }}
              >
                Center
                <br />
                for Digital
                <br />
                Resilience
              </Box>
            </Link>
          </Grid>
          {useCompactHeader && (
            <>
              <Grid item>
                <IconButton onClick={() => setMenuOpen(true)}>
                  <MenuIcon
                    htmlColor={cdrLinkOrange}
                    sx={{ fontSize: 40, mr: -2 }}
                  />
                </IconButton>
              </Grid>
              <SwipeableDrawer
                open={menuOpen}
                onClose={() => {
                  setMenuOpen(false);
                }}
                onOpen={() => {
                  setMenuOpen(true);
                }}
                anchor="right"
              >
                <Grid
                  item
                  container
                  direction="column"
                  spacing={3}
                  alignItems="flex-start"
                  wrap="nowrap"
                  sx={{ p: 2 }}
                >
                  <Grid
                    item
                    container
                    direction="column"
                    alignItems="flex-start"
                    spacing={1}
                    sx={{ borderBottom: "1px solid #000", pb: 2 }}
                  >
                    <Grid item>
                      <Box
                        component="h4"
                        sx={{ ...h4, color: cdrLinkOrange, p: 1, pt: 0, m: 0 }}
                      >
                        {t("caseStudies")}
                      </Box>
                    </Grid>
                    <Grid item>
                      <Link href="/case-studies">
                        <Button variant="text" sx={buttonMenuItemStyles}>
                          {t("overview")}
                        </Button>
                      </Link>
                    </Grid>
                    <Grid item>
                      <Link href="/case-studies/mena">
                        <Button variant="text" sx={buttonMenuItemStyles}>
                          {t("menaRegion")}
                        </Button>
                      </Link>
                    </Grid>
                    <Grid item>
                      <Link href="/case-studies/us">
                        <Button variant="text" sx={buttonMenuItemStyles}>
                          {t("usElections")}
                        </Button>
                      </Link>
                    </Grid>
                  </Grid>
                  <Grid
                    item
                    container
                    direction="column"
                    alignItems="flex-start"
                    spacing={1}
                    sx={{ borderBottom: "1px solid #000", pb: 2 }}
                  >
                    <Grid item>
                      <Box
                        component="h4"
                        sx={{ ...h4, color: cdrLinkOrange, p: 1, pt: 0, m: 0 }}
                      >
                        {t("techSolutions")}
                      </Box>
                    </Grid>
                    <Grid item>
                      <Link href="/solutions">
                        <Button variant="text" sx={buttonMenuItemStyles}>
                          {t("overview")}
                        </Button>
                      </Link>
                    </Grid>
                    <Grid item>
                      <Link href="/solutions/link">
                        <Button variant="text" sx={buttonMenuItemStyles}>
                          {t("cdrLink")}
                        </Button>
                      </Link>
                    </Grid>
                    <Grid item>
                      <Link href="/solutions/leafcutter">
                        <Button variant="text" sx={buttonMenuItemStyles}>
                          {t("leafcutter")}
                        </Button>
                      </Link>
                    </Grid>
                    <Grid item>
                      <Link href="/solutions/wellness-check">
                        <Button variant="text" sx={buttonMenuItemStyles}>
                          {t("wellnessCheck")}
                        </Button>
                      </Link>
                    </Grid>
                    <Grid item>
                      <Link href="/solutions/waterbear">
                        <Button variant="text" sx={buttonMenuItemStyles}>
                          {t("waterbear")}
                        </Button>
                      </Link>
                    </Grid>
                    <Grid item>
                      <Link href="https://docs.digiresilience.org">
                        <Button variant="text" sx={buttonMenuItemStyles}>
                          {t("userGuides")}
                        </Button>
                      </Link>
                    </Grid>
                  </Grid>
                  <Grid
                    item
                    container
                    direction="column"
                    alignItems="flex-start"
                    spacing={1}
                    sx={{ borderBottom: "1px solid #000", pb: 2 }}
                  >
                    <Grid item>
                      <Box
                        component="h4"
                        sx={{ ...h4, color: cdrLinkOrange, p: 1, pt: 0, m: 0 }}
                      >
                        {t("about")}
                      </Box>
                    </Grid>
                    <Grid item>
                      <Link href="/about">
                        <Button variant="text" sx={buttonMenuItemStyles}>
                          {t("overview")}
                        </Button>
                      </Link>
                    </Grid>
                    <Grid item>
                      <Link href="/about#pillars">
                        <Button variant="text" sx={buttonMenuItemStyles}>
                          {t("valuePillars")}
                        </Button>
                      </Link>
                    </Grid>
                    <Grid item>
                      <Link href="/about#team">
                        <Button variant="text" sx={buttonMenuItemStyles}>
                          {t("ourTeam")}
                        </Button>
                      </Link>
                    </Grid>
                  </Grid>
                  <Grid item sx={{ m: "0 auto" }}>
                    <CDRButton
                      color={cdrLinkOrange}
                      text={t("reachOut")}
                      href="mailto:info@digiresilience.org"
                    />
                  </Grid>
                </Grid>
              </SwipeableDrawer>
            </>
          )}
          {!useCompactHeader && (
            <Grid
              item
              container
              direction="row"
              xs={7}
              spacing={useSmallerSize ? 0 : 1}
              alignItems="center"
              justifyContent="flex-end"
              wrap="nowrap"
            >
              <Grid item>
                <Button
                  variant="text"
                  sx={buttonStyles}
                  {...bindTrigger(caseStudiesPopupState)}
                >
                  {t("caseStudies")} <ArrowDropDownIcon />
                </Button>
                <Menu {...bindMenu(caseStudiesPopupState)} elevation={2}>
                  <MenuItem onClick={caseStudiesPopupState.close}>
                    <Link href="/case-studies">
                      <Button variant="text" sx={buttonMenuItemStyles}>
                        {t("overview")}
                      </Button>
                    </Link>
                  </MenuItem>
                  <MenuItem onClick={caseStudiesPopupState.close}>
                    <Link href="/case-studies/mena">
                      <Button variant="text" sx={buttonMenuItemStyles}>
                        {t("menaRegion")}
                      </Button>
                    </Link>
                  </MenuItem>
                  <MenuItem onClick={caseStudiesPopupState.close}>
                    <Link href="/case-studies/us">
                      <Button variant="text" sx={buttonMenuItemStyles}>
                        {t("usElections")}
                      </Button>
                    </Link>
                  </MenuItem>
                </Menu>
              </Grid>
              <Grid item>
                <Button
                  variant="text"
                  sx={buttonStyles}
                  {...bindTrigger(techSolutionsPopupState)}
                >
                  {t("techSolutions")} <ArrowDropDownIcon />
                </Button>
                <Menu {...bindMenu(techSolutionsPopupState)} elevation={2}>
                  <MenuItem onClick={techSolutionsPopupState.close}>
                    <Link href="/solutions">
                      <Button variant="text" sx={buttonMenuItemStyles}>
                        {t("overview")}
                      </Button>
                    </Link>
                  </MenuItem>
                  <MenuItem onClick={techSolutionsPopupState.close}>
                    <Link href="/solutions/link">
                      <Button variant="text" sx={buttonMenuItemStyles}>
                        {t("cdrLink")}
                      </Button>
                    </Link>
                  </MenuItem>
                  <MenuItem onClick={techSolutionsPopupState.close}>
                    <Link href="/solutions/leafcutter">
                      <Button variant="text" sx={buttonMenuItemStyles}>
                        {t("leafcutter")}
                      </Button>
                    </Link>
                  </MenuItem>
                  <MenuItem onClick={techSolutionsPopupState.close}>
                    <Link href="/solutions/wellness-check">
                      <Button variant="text" sx={buttonMenuItemStyles}>
                        {t("wellnessCheck")}
                      </Button>
                    </Link>
                  </MenuItem>
                  <MenuItem onClick={techSolutionsPopupState.close}>
                    <Link href="/solutions/waterbear">
                      <Button variant="text" sx={buttonMenuItemStyles}>
                        {t("waterbear")}
                      </Button>
                    </Link>
                  </MenuItem>
                  <MenuItem onClick={techSolutionsPopupState.close}>
                    <Link href="https://docs.digiresilience.org">
                      <Button variant="text" sx={buttonMenuItemStyles}>
                        {t("userGuides")}
                      </Button>
                    </Link>
                  </MenuItem>
                </Menu>
              </Grid>
              <Grid item>
                <Button
                  variant="text"
                  sx={buttonStyles}
                  {...bindTrigger(aboutPopupState)}
                >
                  {t("about")} <ArrowDropDownIcon />
                </Button>
                <Menu {...bindMenu(aboutPopupState)} elevation={2}>
                  <MenuItem onClick={aboutPopupState.close}>
                    <Link href="/about">
                      <Button variant="text" sx={buttonMenuItemStyles}>
                        {t("overview")}
                      </Button>
                    </Link>
                  </MenuItem>
                  <MenuItem onClick={aboutPopupState.close}>
                    <Link href="/about#pillars">
                      <Button variant="text" sx={buttonMenuItemStyles}>
                        {t("valuePillars")}
                      </Button>
                    </Link>
                  </MenuItem>
                  <MenuItem onClick={aboutPopupState.close}>
                    <Link href="/about#team">
                      <Button variant="text" sx={buttonMenuItemStyles}>
                        {t("ourTeam")}
                      </Button>
                    </Link>
                  </MenuItem>
                </Menu>
              </Grid>
              <Grid item>
                <Link href="/docs/welcome">
                  <Button variant="text" sx={buttonStyles}>
                    {t("docs")}
                  </Button>
                </Link>
              </Grid>
              <Grid item sx={{ ml: 3 }}>
                <CDRButton
                  color={cdrLinkOrange}
                  text={t("reachOut")}
                  href="mailto:info@digiresilience.org"
                />
              </Grid>
            </Grid>
          )}
        </Grid>
      </Container>
    </AppBar>
  );
};
