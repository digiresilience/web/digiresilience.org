"use client";

import { FC } from "react";
import Image from "next/image";
import {
  Container,
  Grid,
  Box,
  Button,
  Hidden,
  useMediaQuery,
  useTheme,
} from "@mui/material";
import Link from "next/link";
import logoSmall from "app/_images/logo-small.png";
import footerLogo from "app/_images/footer-logo.png";
import twitterLogo from "app/_images/twitter-logo.png";
import gitlabLogo from "app/_images/gitlab-logo.png";
import { Subscribe } from "./Subscribe";
import { FooterLinks } from "./FooterLinks";
import { t, loader } from "app/_lib/helpers";
import { colors, typography } from "app/_styles/theme";

export const Footer: FC = () => {
  const { white, bumpedPurple, mutedPurple } = colors;
  const { bodySmall } = typography;

  const theme = useTheme();
  const mobile = useMediaQuery(theme.breakpoints.down("sm"));

  const smallLinkStyles: any = {
    ...bodySmall,
    color: white,
    textTransform: "none",
  };

  return (
    <Grid
      container
      wrap="wrap"
      sx={{
        backgroundColor: bumpedPurple,
        backgroundImage: `url(${loader(footerLogo)})`,
        backgroundBlendMode: "overlay",
        backgroundPosition: "bottom left",
        backgroundRepeat: "no-repeat",
        backgroundSize: "30%",
        width: "100vw",
        overflow: "hidden",
      }}
    >
      <Grid
        item
        container
        wrap={mobile ? "wrap" : "nowrap"}
        xs={12}
        md={6}
        sx={{ order: { xs: 2, md: 1 }, padding: 2 }}
      >
        <Hidden smDown>
          <Grid item sx={{ ml: 2, mr: 4 }}>
            <Image src={logoSmall} alt="CDR logo" width={50} />
          </Grid>
        </Hidden>
        <FooterLinks
          title={t("partners")}
          links={[{ name: t("exampleProjects"), href: "/case-studies" }]}
        />
        <FooterLinks
          title={t("techSolutions")}
          links={[
            { name: t("cdrLink"), href: "/solutions/link" },
            { name: t("waterbear"), href: "/solutions/waterbear" },
            {
              name: t("wellnessCheck"),
              href: "/solutions/wellness-check",
            },
            {
              name: t("userGuides"),
              href: "https://docs.digiresilience.org",
            },
          ]}
        />
        <FooterLinks
          title={t("about")}
          links={[
            { name: t("valuePillars"), href: "/about#pillars" },
            { name: t("ourTeam"), href: "/about#team" },
            { name: t("blog"), href: "/blog" },
            {
              name: t("contactUs"),
              href: "mailto:info@digiresilience.org",
            },
          ]}
        />
      </Grid>
      <Grid xs={12} md={6} item sx={{ order: { xs: 1, md: 2 } }}>
        <Subscribe />
      </Grid>
      {!mobile && (
        <Grid xs={12} item sx={{ backgroundColor: mutedPurple, order: 3 }}>
          <Container>
            <Grid
              item
              container
              direction="row"
              justifyContent="space-between"
              wrap="nowrap"
              alignItems="center"
            >
              <Grid
                item
                container
                direction="row"
                spacing={1}
                alignItems="center"
              >
                <Grid item>
                  <Box component="p" sx={{ ...bodySmall, color: white }}>
                    ©️ {t("copyright")}
                  </Box>
                </Grid>
                <Grid item>
                  <Link href="/about/attributions">
                    <Button variant="text" sx={smallLinkStyles}>
                      {t("attributions")}
                    </Button>
                  </Link>
                </Grid>
                <Grid item>
                  <Link href="/about/privacy">
                    <Button variant="text" sx={smallLinkStyles}>
                      {t("privacyPolicy")}
                    </Button>
                  </Link>
                </Grid>
                <Grid item>
                  <Link href="/about/code-practice">
                    <Button variant="text" sx={smallLinkStyles}>
                      {t("codeOfPractice")}
                    </Button>
                  </Link>
                </Grid>
                <Grid item>
                  <Link href="/about/code-conduct">
                    <Button variant="text" sx={smallLinkStyles}>
                      {t("codeOfConduct")}
                    </Button>
                  </Link>
                </Grid>
              </Grid>
              <Grid item sx={{ p: 1, pl: 0 }}>
                <a href="https://gitlab.com/digiresilience">
                  <Image src={gitlabLogo} alt="Gitlab logo" width={40} />
                </a>
              </Grid>
              <Grid item sx={{ p: 1, pr: 0 }}>
                <a href="https://twitter.com/cdr_tech">
                  <Image src={twitterLogo} alt="Twitter logo" width={40} />
                </a>
              </Grid>
            </Grid>
          </Container>
        </Grid>
      )}
    </Grid>
  );
};
