"use client";

import { FC, PropsWithChildren } from "react";
import { Box } from "@mui/material";

type DiagonalContainerProps = PropsWithChildren<{
  containerColor: string;
  backgroundColor: string;
  clipPath: string;
}>;

export const DiagonalContainer: FC<DiagonalContainerProps> = ({
  containerColor,
  backgroundColor,
  clipPath,
  children,
}) => (
  <Box sx={{ mb: { xs: 2, lg: 6}, background: backgroundColor, overflow: "hidden" }}>
    <Box
      sx={{
        width: "100%",
        padding: {xs: 2, lg: 4 },
        paddingBottom: {xs: 6, lg: 16 },
        clipPath,
        background: containerColor,
      }}
    >
      <Box
        sx={{
          width: "100%",
        }}
      >
        {children}
      </Box>
    </Box>
  </Box>
);
