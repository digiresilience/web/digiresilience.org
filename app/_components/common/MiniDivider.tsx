"use client";

import { FC } from "react";
import { Box, Grid } from "@mui/material";
import { colors } from "app/_styles/theme";

export const MiniDivider: FC = () => {
  const { almostBlack } = colors;

  return (
    <Grid item>
      <Box
        sx={{
          width: "80px",
          height: "6px",
          padding: 0,
          backgroundColor: almostBlack,
          margin: "16px auto",
          mb: "24px",
        }}
      />
    </Grid>
  );
};
