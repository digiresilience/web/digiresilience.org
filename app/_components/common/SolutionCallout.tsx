"use client";

import { FC } from "react";
import Image, { StaticImageData } from "next/image";
import { Card, Grid, Box } from "@mui/material";
import { Button } from "./Button";
import { typography } from "app/_styles/theme";

interface SolutionCalloutProps {
  name: string;
  description: string;
  image: StaticImageData;
  href: string;
  color: string;
  height: number;
}

export const SolutionCallout: FC<SolutionCalloutProps> = ({
  name,
  description,
  image,
  href,
  color,
  height = 290,
}) => {
  const { h3, body } = typography;

  return (
    <Grid item xs={12} sm={6}>
      <Card sx={{ padding: 5, margin: 2, minHeight: height }}>
        <Grid
          container
          direction="column"
          alignItems="center"
          justifyContent="center"
          sx={{ minHeight: height }}
        >
          <Grid item>
            <Image src={image} alt={name} width={100} />
          </Grid>
          <Grid item>
            <Box component="h3" sx={{ ...h3, color }}>
              {name}
            </Box>
          </Grid>
          <Grid item>
            <Box component="p" sx={{ ...body }}>
              {description}
            </Box>
          </Grid>
          <Button text="Learn More" color={color} href={href} />
        </Grid>
      </Card>
    </Grid>
  );
};
