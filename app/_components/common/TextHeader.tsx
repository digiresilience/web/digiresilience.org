"use client";

import { FC } from "react";
import { Box, Container, useMediaQuery, useTheme } from "@mui/material";
import { DiagonalContainer } from "./DiagonalContainer";
import { colors, typography } from "app/_styles/theme";

interface TextHeaderProps {
  text: string;
  backgroundColor?: string;
}

export const TextHeader: FC<TextHeaderProps> = ({
  text,
  backgroundColor = "#ffffff",
}) => {
  const { waterbearElectricPurple, white } = colors;
  const { h1 } = typography;
  
  const theme = useTheme();
  const mobile = useMediaQuery(theme.breakpoints.down('md'));

  return (
    <DiagonalContainer
      containerColor={waterbearElectricPurple}
      backgroundColor={backgroundColor}
      clipPath={`polygon(0% 0%, 100% 0%, 100% ${
        mobile ? "90%" : "70%"
      }, 0% 100%)`}
    >
      <Container>
        <Box
          component="h1"
          sx={{ ...h1, color: white, mt: 4 }}
        >
          {text}
        </Box>
      </Container>
    </DiagonalContainer>
  );
};
