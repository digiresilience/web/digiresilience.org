"use client";

import { FC } from "react";
import Image, { StaticImageData } from "next/image";
import { Grid, Box } from "@mui/material";

interface NetworkPartnerProps {
  name: string;
  image: StaticImageData;
}

export const NetworkPartner: FC<NetworkPartnerProps> = ({ name, image }) => (
  <Grid
    item
    container
    direction="column"
    justifyContent="space-around"
    alignItems="center"
    sm={12}
    md={3}
  >
    <Grid item>
      <Box sx={{ textAlign: "center" }}>
        <Image src={image} alt={name} width={120} />
      </Box>
    </Grid>
  </Grid>
);
