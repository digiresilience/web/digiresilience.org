"use client";

import { FC, PropsWithChildren } from "react";
import { Grid } from "@mui/material";
import { TopNav } from "./TopNav";
import { Footer } from "./Footer";
import { CssBaseline, useTheme, useMediaQuery } from "@mui/material";
import { css, Global } from "@emotion/react";
import { fonts } from "app/_styles/theme";

type LayoutProps = PropsWithChildren<{
  docs?: any;
}>;

export const Layout: FC<LayoutProps> = ({ docs, children }) => {

  const theme = useTheme();
  const mobile = useMediaQuery(theme.breakpoints.down('sm'));

  const { roboto } = fonts;
  const globalCSS = css`
    .rnr-paragraph {
      font-family: ${roboto.style.fontFamily};
      margin: 0px 0px 16px 0px;
    }

    .rnr-image {
      max-width: 100%;
    }

    .rnr-video {
      width: 100%;
      height: 400px;
      margin: 0px auto;
    }

    a {
      color: #ff7115;
      text-decoration: none;
    }

    #notion-renderer a {
      color: #000000;
      text-decoration: none !important;
    }
  `;

  return (
    <>
      <Global styles={globalCSS} />
      <CssBaseline />
      <Grid container direction="column">
        <Grid item>
          <TopNav docs={docs} />
        </Grid>
        <Grid item sx={{ mt: mobile ? "90px" : "120px", width: "100vw", overflow: "hidden" }}>
          {children}
        </Grid>
        <Grid item>
          <Footer />
        </Grid>
      </Grid>
    </>
  );
};
