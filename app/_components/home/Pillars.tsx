"use client";

import { FC } from "react";
import { Container, Grid, Box } from "@mui/material";
import circleEdgePeople from "app/_images/circle-edge-people.png";
import circleCenterPeople from "app/_images/circle-center-people.png";
import openSource from "app/_images/open-source.png";
import { VerticalCallout } from "app/_components/common/VerticalCallout";
import { t } from "app/_lib/helpers";
import { colors, typography } from "app/_styles/theme";

export const Pillars: FC = () => {
  const { cdrLinkOrange } = colors;
  const { h2, h5, body } = typography;

  return (
    <Container>
      <Grid container direction="column" pt={7} pb={7}>
        <Grid item>
          <Box
            component="h2"
            sx={{ ...h2, margin: "0 auto", mb: 5, maxWidth: 700 }}
          >
            {t("systemsChange")}
          </Box>
        </Grid>
        <Grid item>
          <Grid
            container
            direction="row"
            justifyContent="space-around"
            spacing={3}
          >
            <Box
              component="p"
              sx={{ ...body, mb: 0, maxWidth: 700, textAlign: "center" }}
            >
              {t("systemsChangeDescription")}
            </Box>
          </Grid>
        </Grid>
      </Grid>
      <Grid container direction="column" pt={7} pb={7}>
        <Grid item>
          <Box component="h5" sx={{ ...h5, color: cdrLinkOrange }}>
            {t("threePillarApproach")}
          </Box>
        </Grid>
        <Grid item>
          <Box component="h2" sx={{ ...h2, mb: 5 }}>
            {t("resilienceStartsHere")}
          </Box>
        </Grid>
        <Grid item>
          <Grid
            container
            direction="row"
            justifyContent="space-around"
            spacing={3}
          >
            <VerticalCallout
              title={t("communityEngagement")}
              description={t("communityEngagementDescription")}
              image={circleEdgePeople}
            />
            <VerticalCallout
              title={t("informationAnalysis")}
              description={t("informationAnalysisDescription")}
              image={circleCenterPeople}
            />
            <VerticalCallout
              title={t("openSourceTech")}
              description={t("openSourceTechDescription")}
              image={openSource}
            />
          </Grid>
        </Grid>
      </Grid>
    </Container>
  );
};
