"use client";

import { FC } from "react";
import { Container, Grid } from "@mui/material";
import safeSpace from "app/_images/safe-space.png";
import forTruth from "app/_images/for-truth.png";
import { Project } from "./Project";
import { colors } from "app/_styles/theme";
import { t } from "app/_lib/helpers";

export const Projects: FC = () => {
  const { cdrLinkOrange, leafcutterElectricBlue } = colors;

  return (
    <Container>
      <Grid container direction="row">
        <Grid item xs={12} sm={6}>
          <Project
            title={t("creatingSafeSpace")}
            subtitle={t("digitalSecurityProject")}
            color={`${cdrLinkOrange}`}
            image={safeSpace}
            href="/case-studies/mena"
          />
        </Grid>
        <Grid item xs={12} sm={6}>
          <Project
            title={t("fightingForTruth")}
            subtitle={t("disinformationProject")}
            color={`${leafcutterElectricBlue}`}
            image={forTruth}
            href="/case-studies/us"
          />
        </Grid>
      </Grid>
    </Container>
  );
};
