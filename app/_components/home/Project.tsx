"use client";

import { FC } from "react";
import { StaticImageData } from "next/image";
import { Grid, Box } from "@mui/material";
import { Button } from "app/_components/common/Button";
import { loader } from "app/_lib/helpers";
import { colors, typography } from "app/_styles/theme";

interface ProjectProps {
  title: string;
  subtitle: string;
  color: string;
  image: StaticImageData;
  href: string;
}
export const Project: FC<ProjectProps> = ({
  title,
  subtitle,
  color,
  image,
  href,
}) => {
  const { white } = colors;
  const { h1, h5 } = typography;

  return (
    <Grid
      container
      direction="column"
      justifyContent="space-around"
      sx={{
        background: `linear-gradient(90deg, ${color}80, ${color}20), url(${loader(
          image,
        )})`,
        backgroundBlendMode: "burn",
        backgroundPosition: "center",
        backgroundRepeat: "no-repeat",
        backgroundSize: "cover",
        height: "100%",
      }}
      pt={10}
      pb={10}
      pl={3}
      pr={3}
    >
      <Grid
        container
        item
        direction="column"
        alignItems="center"
        alignContent="center"
      >
        <Grid item>
          <Box component="h5" sx={{ ...h5, color: white }}>
            {subtitle}
          </Box>
        </Grid>
        <Grid item>
          <Box
            component="h1"
            sx={{
              ...h1,
              color: white,
            }}
          >
            {title}
          </Box>
        </Grid>
        <Grid item sx={{ paddingTop: 3 }}>
          <Button text="View Project" href={href} color={white} />
        </Grid>
      </Grid>
    </Grid>
  );
};
