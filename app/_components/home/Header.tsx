"use client";

import { FC } from "react";
import Image from "next/image";
import { Container, Grid, Box, useTheme, useMediaQuery } from "@mui/material";
import homeHeader from "app/_images/home-header.png";
import logoLarge from "app/_images/logo-large.svg";
import { Button } from "app/_components/common/Button";
import { DiagonalContainer } from "app/_components/common/DiagonalContainer";
import { t, loader } from "app/_lib/helpers";
import { colors, typography } from "app/_styles/theme";

export const Header: FC = () => {
  const { cdrLinkOrange, coreYellow, white, almostBlack } = colors;
  const { h1, h2 } = typography;
  
  const theme = useTheme();
  const mobile = useMediaQuery(theme.breakpoints.down('sm'));

  return (
    <Grid container direction="column" spacing={0}>
      <Grid item>
        <DiagonalContainer
          containerColor={`linear-gradient(270deg, ${cdrLinkOrange}, ${coreYellow})`}
          backgroundColor={white}
          clipPath="polygon(0% 0%, 100% 0%, 100% 85%, 0% 100%)"
        >
          <Container>
            <Grid
              container
              direction={mobile ? "column" : "row"}
              wrap="nowrap"
              spacing={0}
            >
              <Grid item xs={8} sx={{ zIndex: 2 }}>
                <Box
                  component="h1"
                  sx={{
                    ...h1,
                    color: "white",
                    textAlign: "left",
                    paddingLeft: { xs: 2, lg: 6 } ,
                    paddingTop: { xs: 4, lg: 14 },
                    paddingBottom: 0,
                  }}
                >{t("buildingResilientSystems")}
                </Box>
              </Grid>
              <Grid item xs={4} sx={{ maxHeight: mobile ? 100 : 500, zIndex: 1, marginTop: mobile ? 5 : 0 }}>
                <Image
                  src={logoLarge}
                  alt="CDR logo"
                  style={{ width: "100%", height: mobile ? "100%" : "auto" }}
                />
              </Grid>
            </Grid>
          </Container>
        </DiagonalContainer>
      </Grid>
      <Grid
        item
        container
        direction="column"
        sx={{
          background: mobile
            ? `linear-gradient(270deg, ${cdrLinkOrange}, ${coreYellow})`
            : `linear-gradient(180deg, ${almostBlack}90 100%, ${almostBlack}70 70%), url(${loader(
                homeHeader,
              )})`,
          backgroundPosition: "center",
          backgroundRepeat: "no-repeat",
          backgroundSize: "cover",
          marginTop: { xs: -12, lg: -30 },
        }}
      >
        <Container>
          <Grid item>
            <Box
              component="h2"
              sx={{
                ...h2,
                color: white,
                textAlign: "left",
                padding: { xs: 4, lg: 12 },
                paddingBottom: 0,
                margin: 0,
                mt: { xs: 6, lg: 30 },
              }}
            >
              {t("protectsAndEmpowers")}
            </Box>
          </Grid>
          <Grid item sx={{ padding: 0, paddingLeft: 12, paddingBottom: 12 }}>
            <Button text={t("learnMore")} color={cdrLinkOrange} href="/about" />
          </Grid>
        </Container>
      </Grid>
    </Grid>
  );
};
