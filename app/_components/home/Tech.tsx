"use client";

import { FC } from "react";
import { Container, Grid, Box } from "@mui/material";
import waterbearLogoSmall from "app/_images/waterbear-logo-small.png";
import linkLogoSmall from "app/_images/link-logo-small.png";
import { SolutionCallout } from "app/_components/common/SolutionCallout";
import { MiniDivider } from "app/_components/common/MiniDivider";
import { colors, typography } from "app/_styles/theme";
import { t } from "app/_lib/helpers";

export const Tech: FC = () => {
  const { cdrLinkOrange, waterbearElectricPurple } = colors;
  const { h2 } = typography;

  return (
    <Container>
      <Grid container direction="column" pt={7} pb={7}>
        <Grid item>
          <Box
            component="h2"
            style={{
              ...(h2 as any),
            }}
          >
            {t("techGetThere")}
          </Box>
        </Grid>
        <MiniDivider />
        <Grid item>
          <Grid container direction="row">
            <SolutionCallout
              name={t("waterbear")}
              description={t("waterbearShortDescription")}
              image={waterbearLogoSmall}
              href="/solutions/waterbear"
              color={waterbearElectricPurple}
              height={300}
            />
            <SolutionCallout
              name={t("cdrLink")}
              description={t("cdrLinkShortDescription")}
              image={linkLogoSmall}
              href="/solutions/link"
              color={cdrLinkOrange}
              height={300}
            />
          </Grid>
        </Grid>
      </Grid>
    </Container>
  );
};
