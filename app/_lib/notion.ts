import { Client } from "@notionhq/client";

export const retryableRequest = async (notionCall: any): Promise<any> => {
  const MAX_RETRIES = 5;
  let attempt = 0;

  const execute = async (resolve: any, reject: any) => {
    try {
      const result = await notionCall();
      resolve(result);
    } catch (error) {
      if (error.status === 429) {
        const retryAfter = error.headers?.get("Retry-After") ?? 5;
        console.error(`Rate limited. Retrying after ${retryAfter} seconds...`);

        if (++attempt < MAX_RETRIES) {
          setTimeout(
            () => execute(resolve, reject),
            parseInt(retryAfter) * 1000,
          );
        } else {
          reject(new Error(`Failed after ${MAX_RETRIES} retries.`));
        }
      } else {
        reject(error);
      }
    }
  };

  return new Promise(execute);
}

export const getNotionProperty = async (
  client: any,
  pageID: string,
  propertyID: string,
) => {
  const propertyItem = await retryableRequest(() => client.pages.properties.retrieve({
    page_id: pageID,
    property_id: propertyID,
  }));

  if (propertyItem.object === "property_item") {
    return [propertyItem];
  }

  let nextCursor = propertyItem.next_cursor;
  const { results } = propertyItem;

  while (nextCursor !== null) {
    const propItem = await retryableRequest(() => client.pages.properties.retrieve({
      page_id: pageID,
      property_id: propertyID,
      start_cursor: nextCursor,
    }));

    nextCursor = propItem.next_cursor;
    results.push(...propItem.results);
  }

  return results;
};

export async function getAllDocsPages() {
  const notion = new Client({ auth: process.env.NOTION_API_KEY });
  const databaseID = process.env.NOTION_DOCS_DATABASE_ID!;
  const response = await retryableRequest(() => notion.databases.query({
    database_id: databaseID,
    filter: {
      and: [
        {
          property: "Published",
          checkbox: {
            equals: true,
          },
        },
      ],
    },
    sorts: [
      {
        property: "Section",
        direction: "ascending",
      },
      {
        property: "Position",
        direction: "ascending",
      },
    ],
  }));

  const docs = await Promise.all(
    response.results.map(async (result: any) => {
      const properties: any = {};

      for await (const key of Object.keys(result.properties)) {
        const val = result.properties[key];
        const prop = await getNotionProperty(notion, result.id, val.id);
        properties[key] = prop;
      }

      return {
        id: result.id,
        section: properties.Section[0].rich_text.plain_text,
        title: properties.Name[0].title.plain_text,
        path: properties.Path[0].rich_text.plain_text,
        position: properties.Position[0].number,
      };
    }),
  );

  return docs;
}

export async function getDocPage(doc: string[]) {
  const notion = new Client({ auth: process.env.NOTION_API_KEY });
  const databaseID = process.env.NOTION_DOCS_DATABASE_ID!;
  const response = await retryableRequest(() => notion.databases.query({
    database_id: databaseID,
    filter: {
      and: [
        {
          property: "Published",
          checkbox: {
            equals: true,
          },
        },
        {
          property: "Path",
          rich_text: {
            equals: doc.join("/"),
          },
        },
      ],
    },
  }));

  const pages = await Promise.all(
    response.results.map(async (result: any) => {
      const blockRequest = await retryableRequest(() => notion.blocks.children.list({
        block_id: result.id,
        page_size: 50,
      }));

      const properties: any = {};

      for await (const key of Object.keys(result.properties)) {
        const val = result.properties[key];
        const prop = await getNotionProperty(notion, result.id, val.id);
        properties[key] = prop;
      }

      return {
        id: result.id,
        title: properties.Name[0].title.plain_text,
        text: blockRequest.results,
      };
    }),
  );

  const page = pages[0];

  return page;
}
