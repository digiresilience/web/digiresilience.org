import en from "app/_locales/en.json";

export const t = (key: string) => {
  return en[key] ?? key;
};

export const loader = (image: any) => `${image.src}?${image.width ?? ""}`;
