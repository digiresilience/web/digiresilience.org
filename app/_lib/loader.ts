"use client";

import { loader } from "./helpers";

export default function imageLoader(image: any) {
  return loader(image);
}
